<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-right image">
                <img src="{{ asset('admin/dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-right info">
                <p>علیرضا حسینی زاده</p>
                <a href="#"><i class="fa fa-circle text-success"></i> آنلاین</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="جستجو">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">منو</li>
            <li class="active treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>داشبرد</span>
                    <span class="pull-left-container">
              <i class="fa fa-angle-right pull-left"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="active"><a href="index.html"><i class="fa fa-circle-o"></i> داشبرد اول</a></li>
                    <li><a href="index2.html"><i class="fa fa-circle-o"></i> داشبرد دوم</a></li>
                </ul>
            </li>

            <li class="active treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>نقش ها</span>
                    <span class="pull-left-container">
              <i class="fa fa-angle-right pull-left"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="active"><a href="{{ route('admin.role.create') }}"><i class="fa fa-circle-o"></i>افزودن نقش </a></li>
                    <li><a href="{{ route('admin.role.index') }}"><i class="fa fa-circle-o"></i>لیست نقش ها</a></li>

                    <li class="active"><a href="{{ route('admin.permission.create') }}"><i class="fa fa-circle-o"></i>افزودن دسترسی </a></li>
                    <li><a href="{{ route('admin.permission.index') }}"><i class="fa fa-circle-o"></i>لیست دسترسی ها</a></li>

                    <li><a href="{{ route('admin.permissionuser.create') }}"><i class="fa fa-circle-o"></i>افزودن دسترسی کاربر</a></li>
                    <li><a href="{{ route('admin.permissionuser.index') }}"><i class="fa fa-circle-o"></i>لیست دسترسی کاربر</a></li>

                    <li><a href="{{ route('admin.roleuser.create') }}"><i class="fa fa-circle-o"></i>افزودن نقش کاربر</a></li>
                    <li><a href="{{ route('admin.roleuser.index') }}"><i class="fa fa-circle-o"></i>لیست نقش کاربر</a></li>

                </ul>
            </li>

            <li class="active treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>پیام ها</span>
                    <span class="pull-left-container">
              <i class="fa fa-angle-right pull-left"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="active"><a href="{{ route('admin.message.create') }}"><i class="fa fa-circle-o"></i>افزودن پیام </a></li>
                    <li><a href="{{ route('admin.message.index') }}"><i class="fa fa-circle-o"></i>لیست پیام ها</a></li>
                </ul>
            </li>

            <li class="active treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>محصول ها</span>
                    <span class="pull-left-container">
              <i class="fa fa-angle-right pull-left"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="active"><a href="{{ route('admin.productcategories.create') }}"><i class="fa fa-circle-o"></i>افزودن دسته بندی محصول </a></li>
                    <li><a href="{{ route('admin.productcategories.index') }}"><i class="fa fa-circle-o"></i>لیست دسته بندی محصول ها</a></li>

                    <li class="active"><a href="{{ route('admin.product.create') }}"><i class="fa fa-circle-o"></i>افزودن محصول </a></li>
                    <li><a href="{{ route('admin.product.index') }}"><i class="fa fa-circle-o"></i>لیست محصول ها</a></li>

                    <li class="active"><a href="{{ route('admin.productgalleries.create') }}"><i class="fa fa-circle-o"></i>افزودن گالری محصول </a></li>

                </ul>
            </li>

            <li class="active treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>ویژگی محصول ها</span>
                    <span class="pull-left-container">
              <i class="fa fa-angle-right pull-left"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="active"><a href="{{ route('admin.categoryfeatures.create') }}"><i class="fa fa-circle-o"></i>افزودن ویژگی </a></li>
                    <li><a href="{{ route('admin.categoryfeatures.index') }}"><i class="fa fa-circle-o"></i>لیست ویژگی ها</a></li>

                    <li class="active"><a href="{{ route('admin.featurevalues.create') }}"><i class="fa fa-circle-o"></i>افزودن مقادیر ویژگی </a></li>
                    <li><a href="{{ route('admin.featurevalues.index') }}"><i class="fa fa-circle-o"></i>لیست مقادیر ویژگی</a></li>

                    <li class="active"><a href="{{ route('admin.featureitems.create') }}"><i class="fa fa-circle-o"></i>افزودن ویژگی به دسته بندی </a></li>
                    <li><a href="{{ route('admin.featureitems.index') }}"><i class="fa fa-circle-o"></i>لیست ویژگی دسته بندی </a></li>
                </ul>
            </li>

            <li class="active treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>پکیج</span>
                    <span class="pull-left-container">
              <i class="fa fa-angle-right pull-left"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="active"><a href="{{ route('admin.package.create') }}"><i class="fa fa-circle-o"></i>افزودن پکیج </a></li>
                    <li><a href="{{ route('admin.package.index') }}"><i class="fa fa-circle-o"></i>لیست پکیج ها</a></li>
                    <li><a href="{{ route('admin.packageproduct.create') }}"><i class="fa fa-circle-o"></i>افزودن محصول به پکیج</a></li>
                </ul>
            </li>

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
