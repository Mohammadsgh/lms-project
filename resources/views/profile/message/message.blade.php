@extends('master.profile')

@section('title') پروفایل @endsection

@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                پروفایل کاربری
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> خانه</a></li>
                <li><a href="#">مثال ها</a></li>
                <li class="active"> پروفایل</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-3">

                    <!-- Profile Image -->
                    <div class="box box-primary">
                        <div class="box-body box-profile">
                            <img class="profile-user-img img-responsive img-circle"
                                 src="/admin/dist/img/user4-128x128.jpg" alt="User profile picture">

                            <h3 class="profile-username text-center">{{ Auth::user()->username }}</h3>

                            <p class="text-muted text-center">مهندس نرم افزار</p>

                            <ul class="list-group list-group-unbordered">
                                <li class="list-group-item">
                                    <b>پیام ها</b> <a href="#messages" class="pull-left">پیام ها</a>
                                </li>
                                <li class="list-group-item">
                                    <b>دنبال کننده</b> <a class="pull-left">543</a>
                                </li>
                                <li class="list-group-item">
                                    <b>دوستان</b> <a class="pull-left">13,287</a>
                                </li>
                            </ul>

                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                خروج
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                </div>
                <!-- /.col -->
                <div class="col-md-9">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li><a href="#activity" data-toggle="tab">فعالیت ها</a></li>
                            <li class="active"><a href="#messages" data-toggle="tab">پیام ها</a></li>
                            <li><a href="#settings" data-toggle="tab">تنظیمات</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="active tab-pane" id="messages">
                                <!-- Post -->

                                @if(count($messages) > 0)
                                    @foreach($messages as $message)
                                        <div class="post">
                                            <div class="user-block">

                                        <span class="username">
                                          <a href="#">{{ $message->title }}</a>

                                        </span>
                                                <span class="description">ارسال شده در {{ gregorianToJalali($message->created_at) }}</span>
                                            </div>
                                            <!-- /.user-block -->
                                            <p>
                                                {{ $message->content }}
                                            </p>

                                        </div>

                                        {{ $messages->links() }}

                                    @endforeach
                                @else
                                    <p>هیچ پیامی وجود ندارد</p>
                                @endif

                            <!-- /.post -->

                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="as">
                                <!-- The timeline -->
                                <ul class="timeline timeline-inverse">
                                    <!-- timeline time label -->
                                    <li class="time-label">
                  <span class="bg-red">
                    ۱۲ اردیبهشت ۱۳۹۴
                  </span>
                                    </li>
                                    <!-- /.timeline-label -->
                                    <!-- timeline item -->
                                    <li>
                                        <i class="fa fa-envelope bg-blue"></i>

                                        <div class="timeline-item">
                                            <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>

                                            <h3 class="timeline-header"><a href="#">تیم پشتیبانی</a> یک ایمیل برای شما
                                                ارسال کرد</h3>

                                            <div class="timeline-body">
                                                لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده
                                                از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و
                                                سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای
                                                متنوع با هدف بهبود ابزارهای کاربردی می باشد.
                                            </div>
                                            <div class="timeline-footer">
                                                <a class="btn btn-primary btn-xs">ادامه</a>
                                                <a class="btn btn-danger btn-xs">حذف</a>
                                            </div>
                                        </div>
                                    </li>

                                </ul>
                            </div>
                            <!-- /.tab-pane -->

                            <div class="tab-pane" id="settings">
                                <form class="form-horizontal">
                                    <div class="form-group">
                                        <label for="inputName" class="col-sm-2 control-label">نام</label>

                                        <div class="col-sm-10">
                                            <input type="email" class="form-control" id="inputName" placeholder="نام">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail" class="col-sm-2 control-label">ایمیل</label>

                                        <div class="col-sm-10">
                                            <input type="email" class="form-control" id="inputEmail"
                                                   placeholder="ایمیل">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputName" class="col-sm-2 control-label">نام خانوادگی</label>

                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="inputName"
                                                   placeholder="نام خانوادگی">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputExperience" class="col-sm-2 control-label">ویژگی ها</label>

                                        <div class="col-sm-10">
                                            <textarea class="form-control" id="inputExperience"
                                                      placeholder="ویژگی ها"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputSkills" class="col-sm-2 control-label">توانایی ها</label>

                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="inputSkills"
                                                   placeholder="توانایی ها">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox"> من <a href="#">قوانین و شرایط</a> را قبول
                                                    میکنم.
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <button type="submit" class="btn btn-danger">ارسال</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- /.nav-tabs-custom -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

        </section>
        <!-- /.content -->
    </div>

@endsection











