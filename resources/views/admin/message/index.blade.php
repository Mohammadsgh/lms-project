@extends('master.admin')

@section('title') لیست پیام ها @endsection

@section('breadcrumbs')
    <section class="content-header">
        <h1>
            داشبرد
            <small>کنترل پنل</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> خانه</a></li>
            <li class="active">داشبرد</li>
        </ol>
    </section>
@endsection

@section('content')

    <div class="row">
        <section class="col-lg-12 col-md-12">

            @include('master.admin_message')

            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">لیست پیام ها</h3>
                </div>
                <!-- /.box-header -->

                <div class="box-body">

                    <table class="table table-bordered">
                        <tbody>

                        <tr>
                            <th style="width: 10px">#</th>
                            <th>عنوان</th>
                            <th>گیرنده</th>
                            <th>نوشته</th>
                        </tr>

                        @foreach($messages as $message)
                            <tr>
                                <td>{{ $message->id }}</td>
                                <td>{{ $message->title }}</td>
                                <td>{{ $message->username }}</td>
                                <td>{{ $message->content }}</td>

                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    {{--<ul class="pagination pagination-sm no-margin pull-right">--}}
                    {!! $messages->links() !!}
                    {{--</ul>--}}
                </div>
            </div>

        </section>
    </div>

@endsection

