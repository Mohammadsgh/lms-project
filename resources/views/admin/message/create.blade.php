@extends('master.admin')

@section('title') فرستادن پیام @endsection

@section('breadcrumbs')
<section class="content-header">
    <h1>
        داشبرد
        <small>کنترل پنل</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> خانه</a></li>
        <li class="active">داشبرد</li>
    </ol>
</section>
@endsection

    @push('styles')
       <link rel="stylesheet" href="/admin/bower_components/select2/dist/css/select2.min.css">
    @endpush

@section('content')

    <section class="content">


        <!-- /.row -->
        <div class="row">
            <section class="col-lg-12 col-md-12">

                @include('master.admin_message')

                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">فرستادن پیام</h3>
                    </div>
                    
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" action="{{ route('admin.message.store') }}" method="post">

                        {{ csrf_field() }}

                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">عنوان</label>

                                <div class="col-sm-10">
                                    <input type="text" name="title" class="form-control" placeholder="عنوان">
                                </div>
                            </div>

                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">روش</label>

                                <div class="col-sm-10">
                                    <select name="type" id="type" class="form-control">
                                            <option value="0">انتخاب کنید</option>
                                            <option value="1">تکی</option>
                                            <option value="2">گروهی</option>
                                    </select>
                                </div>
                            </div>

                        </div>

                        <div class="box-body"><div style="display: none" id="show-type"></div></div>

                        <div class="box-body" style="display: none" id="show-user-lists">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">لیست کاربران</label>

                                <div class="col-sm-10">
                                    <select name="user_id[]" id="users-select" style="width: 100%" class="form-control select2" multiple="multiple" data-placeholder="Select a State">
                                        <option>انتخاب کنید</option>
                                    </select>
                                </div>
                            </div>

                        </div>


                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">نوشته</label>

                                <div class="col-sm-10">
                                    <textarea id="editor1" name="content" rows="10" cols="80" style="visibility: hidden; display: none;"></textarea>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-info pull-right">فرستادن</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
            </section>
        </div>

    </section>
@endsection

@push('scripts')
    <script src="/admin/bower_components/ckeditor/ckeditor.js"></script>
    <script src="/admin/bower_components/select2/dist/js/select2.full.min.js"></script>

   <script>

       $(function () {
           CKEDITOR.replace('editor1');
           $('.select2').select2();
       })

       $(document).ready(function () {

           $('#type').on('change' , function () {
               var type = $('#type option:selected').val();

               if (type == 1)
               {

                   $.ajax({
                       url: '{{ route('admin.user.lists') }}',
                       type: "get",
                       dataType: "json",
                       success: function(data) {

                           var message = '<div class="form-group">' +
                               '<label for="inputEmail3" class="col-sm-2 control-label">لیست کاربران</label>' +
                               '<div class="col-sm-10"><select name="user_id" class="form-control"><option>انتخاب کنید</option>';

                           $.each(data, function (key, value) {
                               message += '<option value="'+ value.id +'">' + value.username + '</option>';
                           });

                           message += '</select></div></div>';

                           $("#show-user-lists").hide();
                           $("#show-type").empty();
                           $("#show-type").html(message);
                           $("#show-type").show();
                       },
                       error: function (data){
                           var errors = data.responseJSON;
                           var html = '<ul>';
                           html += '<b>خطاهای رخ داده:</b>';
                           $.each(errors, function (key, value) {
                               html += '<li class="error">' + value + '</li>';
                           });
                           html += '</ul>';
                           $("#message").empty();
                           $("#errors").html(html);
                       }
                   });

               } else {

                   $.ajax({
                       url: '{{ route('admin.user.lists') }}',
                       type: "get",
                       dataType: "json",
                       success: function(data) {

                           var message = '<option>انتخاب کنید</option>';

                               $.each(data, function (key, value) {
                                   message += '<option value="'+ value.id +'">' + value.username + '</option>';
                               });


                           $("#show-type").hide();
                           $("#users-select").empty();
                           $("#users-select").html(message);
                           $("#show-user-lists").show();
                       },
                       error: function (data){
                           var errors = data.responseJSON;
                           var html = '<ul>';
                           html += '<b>خطاهای رخ داده:</b>';
                           $.each(errors, function (key, value) {
                               html += '<li class="error">' + value + '</li>';
                           });
                           html += '</ul>';
                           $("#message").empty();
                           $("#errors").html(html);
                       }
                   });

               }
           })
       });




   </script>
    @endpush