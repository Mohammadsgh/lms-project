@extends('master.admin')


@section('title') افزودن نقش کاربر @endsection

@section('breadcrumbs')
    <section class="content-header">
        <h1>
            داشبرد
            <small>کنترل پنل</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> خانه</a></li>
            <li class="active">داشبرد</li>
        </ol>
    </section>
@endsection

@section('content')

    <section class="content">


        <!-- /.row -->
        <div class="row">
            <section class="col-lg-12 col-md-12">

                @include('master.admin_message')

                <div class="box box-info">

                    <div class="box-header with-border">
                        <h3 class="box-title">نقش های کاربر</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" action="{{ route('admin.roleuser.store') }}" method="post">

                        {{ csrf_field() }}

                        <div class="form-group">
                            <div class="col-sm-6">
                                <label for="">لیست کاربران</label>

                                <select name="id" class="form-control">
                                    @foreach($users as $user)
                                        <option value="{{ $user->id }}">{{ $user->username }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row">

                        @foreach($roles as $role)

                            <div class="col-sm-2">
                                <div class="checkbox">
                                    <label>
                                        <input name="role[]" value="{{ $role->id }}" type="checkbox"> {{ $role->display_name }}
                                    </label>
                                </div>
                            </div>

                       @endforeach
                        </div>

                    <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-info pull-right">فرستادن</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
            </section>
        </div>

    </section>
@endsection
