@extends('master.admin')


@section('title') افزودن دسترسی نقش @endsection

@section('breadcrumbs')
    <section class="content-header">
        <h1>
            داشبرد
            <small>کنترل پنل</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> خانه</a></li>
            <li class="active">داشبرد</li>
        </ol>
    </section>
@endsection

@section('content')

    <section class="content">


        <!-- /.row -->
        <div class="row">
            <section class="col-lg-12 col-md-12">

                @include('master.admin_message')

                <div class="box box-info">

                    <div class="box-header with-border">
                        <h3 class="box-title">{{ $role->display_name }}</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" action="{{ route('admin.permissionrole.store') }}" method="post">

                        <input type="hidden" name="id" value="{{ $role->id }}">

                        {{ csrf_field() }}

                        <div class="row">

                        @foreach($permissions as $permission)

                            <div class="col-sm-2">
                                <div class="checkbox">
                                    <label>
                                        <input name="permission[]" value="{{ $permission->id }}" type="checkbox"> {{ $permission->display_name }}
                                    </label>
                                </div>
                            </div>

                       @endforeach
                        </div>

                    <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-info pull-right">فرستادن</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
            </section>
        </div>

    </section>
@endsection
