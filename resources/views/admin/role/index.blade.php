@extends('master.admin')

@section('title') لیست نقش ها @endsection

@section('breadcrumbs')
    <section class="content-header">
        <h1>
            داشبرد
            <small>کنترل پنل</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> خانه</a></li>
            <li class="active">داشبرد</li>
        </ol>
    </section>
@endsection

@section('content')

    <div class="row">
        <section class="col-lg-12 col-md-12">

            @include('master.admin_message')

            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">لیست نقش ها</h3>
                </div>
                <!-- /.box-header -->

                <div class="box-body">

                    <table class="table table-bordered">
                        <tbody>

                        <tr>
                            <th style="width: 10px">#</th>
                            <th>نام</th>
                            <th>نام نمایشی</th>
                            <th>توضیحات</th>
                            <th>عملیات</th>
                        </tr>

                        @foreach($roles as $role)
                            <tr>
                                <td>{{ $role->id }}</td>
                                <td>{{ $role->name }}</td>
                                <td>{{ $role->display_name }}</td>
                                <td>{{ $role->description }}</td>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <a href="{{ route('admin.role.edit' , [$role->id]) }}" class="btn btn-primary">ویرایش</a>
                                        </div>

                                        <div class="col-sm-4">
                                            <a href="{{ route('admin.role.destroy' , [$role->id]) }}" class="btn btn-danger">پاک کردن</a>
                                        </div>

                                        <div class="col-sm-4">
                                            <a href="{{ route('admin.permissionrole.show' , [$role->id]) }}" class="btn btn-success">دسترسی ها</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    {{--<ul class="pagination pagination-sm no-margin pull-right">--}}
                    {!! $roles->links() !!}
                    {{--</ul>--}}
                </div>
            </div>

        </section>
    </div>

@endsection

