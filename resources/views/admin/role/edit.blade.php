@extends('master.admin')


@section('title') ویرایش نقش @endsection

@section('breadcrumbs')
<section class="content-header">
    <h1>
        داشبرد
        <small>کنترل پنل</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> خانه</a></li>
        <li class="active">داشبرد</li>
    </ol>
</section>
@endsection

@section('content')

    <section class="content">


        <!-- /.row -->
        <div class="row">
            <section class="col-lg-12 col-md-12">

                @include('master.admin_message')

                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">ویرایش نقش</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" action="{{ route('admin.role.update' , $role->id) }}" method="post">

                        <input name="_method" value="PUT" type="hidden">

                        {{ csrf_field() }}

                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">نام</label>

                                <div class="col-sm-10">
                                    <input type="text" name="name" value="{{ $role->name }}" class="form-control" placeholder="نقش">
                                </div>
                            </div>

                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">نام نمایشی</label>

                                <div class="col-sm-10">
                                    <input type="text" value="{{ $role->display_name }}" name="display_name" class="form-control" placeholder="نام نمایشی">
                                </div>
                            </div>

                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">توضیحات</label>

                                <div class="col-sm-10">
                                    <input type="text" value="{{ $role->description }}" name="description" class="form-control" placeholder="توضیحات">
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-info pull-right">فرستادن</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
            </section>
        </div>

    </section>
@endsection
