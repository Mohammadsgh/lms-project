@extends('master.admin')

@section('title') لیست نقش ها @endsection

@section('breadcrumbs')
    <section class="content-header">
        <h1>
            داشبرد
            <small>کنترل پنل</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> خانه</a></li>
            <li class="active">داشبرد</li>
        </ol>
    </section>
@endsection

@section('content')

    <div class="row">
        <section class="col-lg-12 col-md-12">

            @include('master.admin_message')

            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">لیست نقش ها</h3>
                </div>
                <!-- /.box-header -->

                <div class="box-body">

                    <table class="table table-bordered">
                        <tbody>

                        <tr>
                            <th style="width: 10px">#</th>
                            <th>نام</th>
                            <th>نام نمایشی</th>
                            <th>توضیحات</th>
                            <th>عملیات</th>
                        </tr>

                        @foreach($permissions as $permission)
                            <tr>
                                <td>{{ $permission->id }}</td>
                                <td>{{ $permission->name }}</td>
                                <td>{{ $permission->display_name }}</td>
                                <td>{{ $permission->description }}</td>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <a href="{{ route('permission.edit' , [$permission->id]) }}" class="btn btn-primary">ویرایش</a>
                                        </div>

                                        <div class="col-sm-6">
                                            <a href="{{ route('admin.permission.destroy' , [$permission->id]) }}" class="btn btn-danger">پاک کردن</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    {{--<ul class="pagination pagination-sm no-margin pull-right">--}}
                    {!! $permissions->links() !!}
                    {{--</ul>--}}
                </div>
            </div>

        </section>
    </div>

@endsection

