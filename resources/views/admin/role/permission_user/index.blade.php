@extends('master.admin')

@section('title') لیست دسترسی کاربر @endsection

@section('breadcrumbs')
    <section class="content-header">
        <h1>
            داشبرد
            <small>کنترل پنل</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> خانه</a></li>
            <li class="active">داشبرد</li>
        </ol>
    </section>
@endsection

@section('content')

    <div class="row">
        <section class="col-lg-12 col-md-12">

            @include('master.admin_message')

            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">لیست دسترسی کاربر</h3>
                </div>
                <!-- /.box-header -->

                <div class="box-body">

                    <table class="table table-bordered">
                        <tbody>

                        <tr>
                            <th style="width: 10px">#</th>
                            <th>نام کاربری</th>
                            <th>نام دسترسی</th>
                            <th>عملیات</th>
                        </tr>

                        <?php $i = 1; ?>
                        @foreach($permissionUsers as $permissionUser)
                            <tr>
                                <td>{{ $i }}</td>
                                <td>{{ $permissionUser->username }}</td>
                                <td>{{ $permissionUser->display_name }}</td>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <a href="{{ route('admin.permissionuser.destroy' , [$permissionUser->permission_id , $permissionUser->user_id]) }}" class="btn btn-danger">پاک کردن</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <?php $i++; ?>
                        @endforeach

                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    {{--<ul class="pagination pagination-sm no-margin pull-right">--}}
                    {{ $permissionUsers->links() }}
                    {{--</ul>--}}
                </div>
            </div>

        </section>
    </div>

@endsection

