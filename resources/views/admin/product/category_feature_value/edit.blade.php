@extends('master.admin')

@section('title') ویرایش ویژگی دسته بندی @endsection

@section('breadcrumbs')
<section class="content-header">
    <h1>
        داشبرد
        <small>کنترل پنل</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> خانه</a></li>
        <li class="active">داشبرد</li>
    </ol>
</section>
@endsection

@push('styles')
    <link rel="stylesheet" href="/admin/bower_components/select2/dist/css/select2.min.css">
@endpush

@section('content')

    <section class="content">


        <!-- /.row -->
        <div class="row">
            <section class="col-lg-12 col-md-12">

                @include('master.admin_message')

                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">ویرایش ویژگی دسته بندی</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" enctype="multipart/form-data" action="{{ route('admin.featurevalues.update' , $featureValue->id) }}" method="post">

                        <input name="_method" value="PUT" type="hidden">

                        {{ csrf_field() }}

                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">نام</label>

                                <div class="col-sm-10">
                                    <input type="text" value="{{ $featureValue->name }}" name="name" class="form-control" placeholder="نام">
                                </div>
                            </div>

                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">ویژگی ها</label>

                                <div class="col-sm-10">
                                    <select class="form-control" name="feature_id">
                                        @foreach($features as $feature)
                                            <option value="{{ $feature->id }}">{{ $feature->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">عکس</label>

                                <div class="col-sm-10">
                                    <input type="file" name="image">
                                </div>
                            </div>

                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">توضیحات</label>

                                <div class="col-sm-10">
                                    <textarea class="form-control" name="description">{{ $featureValue->description }}</textarea>
                                </div>
                            </div>

                        </div>

                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-info pull-right">فرستادن</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
            </section>
        </div>

    </section>
@endsection

@push('scripts')
    <script src="/admin/bower_components/select2/dist/js/select2.full.min.js"></script>

    <script>

        $(function () {
            $('.select2').select2({
                dir: 'rtl'
            });
        })

    </script>
@endpush
