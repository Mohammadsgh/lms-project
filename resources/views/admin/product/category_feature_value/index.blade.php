@extends('master.admin')

@section('title') لیست مقدار ویژگی ها @endsection

@section('breadcrumbs')
    <section class="content-header">
        <h1>
            داشبرد
            <small>کنترل پنل</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> خانه</a></li>
            <li class="active">داشبرد</li>
        </ol>
    </section>
@endsection

@section('content')

    <div class="row">
        <section class="col-lg-12 col-md-12">

            @include('master.admin_message')

            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">لیست مقدار ویژگی ها</h3>
                </div>
                <!-- /.box-header -->

                <div class="box-body">

                    <table class="table table-bordered">
                        <tbody>

                        <tr>
                            <th style="width: 10px">#</th>
                            <th>نام</th>
                            <th>عکس</th>
                            <th>ویژگی</th>
                            <th>توضیحات</th>
                            <th>عملیات</th>
                        </tr>

                        @foreach($featureValues as $featureValue)
                            <tr>
                                <td>{{ $featureValue->id }}</td>
                                <td>{{ $featureValue->name }}</td>
                                <td>
                                    @if(! empty($featureValue->image))
                                        <img src="/{{ $featureValue->image }}">
                                    @else
                                        ندارد
                                    @endif
                                </td>
                                <td>{{ $featureValue->feature->name }}</td>
                                <td>{{ $featureValue->description }}</td>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <a href="{{ route('admin.featurevalues.edit' , [$featureValue->id]) }}"
                                               class="btn btn-primary">ویرایش</a>
                                        </div>

                                        <div class="col-sm-6">
                                            <a href="{{ route('admin.featurevalues.destroy' , [$featureValue->id]) }}"
                                               class="btn btn-danger">پاک کردن</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">
                        {{ $featureValues->links() }}
                    </ul>
                </div>
            </div>

        </section>
    </div>

@endsection

