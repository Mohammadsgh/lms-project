@extends('master.admin')

@section('title') فرستادن پیام @endsection

@section('breadcrumbs')
    <section class="content-header">
        <h1>
            داشبرد
            <small>کنترل پنل</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> خانه</a></li>
            <li class="active">داشبرد</li>
        </ol>
    </section>
@endsection

@push('styles')
    <link rel="stylesheet" href="/admin/bower_components/select2/dist/css/select2.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
@endpush

@section('content')

    <section class="content">


        <!-- /.row -->
        <div class="row">
            <section class="col-lg-12 col-md-12">

                @include('master.admin_message')

                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">افزودن ویژگی به دسته بندی</h3>
                    </div>

                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" action="{{ route('admin.featureitems.store') }}" method="post">

                        {{ csrf_field() }}

                        <div class="box-body" id="show-user-lists">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">دسته بندی ها</label>

                                <div class="col-sm-10">
                                    <select multiple="multiple" name="categories[]" class="categories form-control"></select>
                                </div>
                            </div>

                        </div>

                        <div class="box-body" id="show-user-lists">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">ویژگی ها</label>

                                <div class="col-sm-10">

                                    <select name="featureValues[]" class="selectpicker" multiple>
                                        @foreach($features as $feature)
                                            <optgroup label="{{ $feature->name }}" data-max-options="2">
                                                @foreach($feature['feature_value'] as $featureValue)
                                                    <option value="{{ $featureValue->id }}">{{ $featureValue->name }}</option>
                                                @endforeach
                                            </optgroup>
                                        @endforeach
                                    </select>


                                </div>
                            </div>

                        </div>

                    <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-info pull-right">فرستادن</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
            </section>
        </div>

    </section>
@endsection

@push('scripts')
    <script src="/admin/bower_components/select2/dist/js/select2.full.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>

    <script>

        // $(function () {
        $(document).ready(function () {

            $('.categories').select2({
                dir: 'rtl',
                tags: true,
                ajax: {
                    url: '{{ route('admin.featureitems.showcategories') }}',
                    dataType: 'json',
                    delay: 250,
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                }
            });

            $('.selectpicker').selectpicker({
                style: 'btn-info',
                size: 4
            });

        });
        // })

    </script>
@endpush