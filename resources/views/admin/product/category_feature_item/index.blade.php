@extends('master.admin')

@section('title') لیست ویژگی دسته بندی @endsection

@section('breadcrumbs')
    <section class="content-header">
        <h1>
            داشبرد
            <small>کنترل پنل</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> خانه</a></li>
            <li class="active">داشبرد</li>
        </ol>
    </section>
@endsection

@section('content')

    <div class="row">
        <section class="col-lg-12 col-md-12">

            @include('master.admin_message')

            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">لیست ویژگی دسته بندی</h3>
                </div>
                <!-- /.box-header -->

                <div class="box-body">

                    <table class="table table-bordered">
                        <tbody>

                        <tr>
                            <th style="width: 10px">#</th>
                            <th>دسته بندی</th>
                            <th>عملیات</th>
                        </tr>

                        @foreach($categoryFeatureItems as $categoryFeatureItem)
                            <tr>
                                <td>{{ $categoryFeatureItem->id }}</td>
                                <td>{{ $categoryFeatureItem->name }}</td>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <a href="{{ route('admin.featureitems.edit' , [$categoryFeatureItem->id]) }}" class="btn btn-primary">ویرایش</a>
                                        </div>

                                        <div class="col-sm-2">
                                            <a href="{{ route('admin.featureitems.show' , [$categoryFeatureItem->id]) }}" class="btn btn-success">نمایش ویژگی ها</a>
                                        </div>

                                    </div>
                                </td>

                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">
                    {{ $categoryFeatureItems->links() }}
                    </ul>
                </div>
            </div>

        </section>
    </div>

@endsection

