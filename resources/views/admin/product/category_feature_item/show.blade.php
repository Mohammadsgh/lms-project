@extends('master.admin')

@section('title') لیست ویژگی ها @endsection

@section('breadcrumbs')
    <section class="content-header">
        <h1>
            داشبرد
            <small>کنترل پنل</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> خانه</a></li>
            <li class="active">داشبرد</li>
        </ol>
    </section>
@endsection

@section('content')

    <div class="row">
        <section class="col-lg-12 col-md-12">

            @include('master.admin_message')

            <div class="box box-info">

                <div class="box-body">

                    <h4>
                        <strong>{{ $feature['name'] }}</strong>
                    </h4>

                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>نام ویژگی</th>
                            <th>مقادیر</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($feature['category_feature_values'] as $item )
                            <tr>
                                <td>{{ $item['name'] }}</td>
                                <td>
                                    <div class="row">
                                        @foreach($item['feature_value'] as $value)
                                            <div class="col-sm-2">
                                                {{ $value['name'] }}
                                            </div>
                                        @endforeach
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>

            </div>

        </section>
    </div>

@endsection

