@extends('master.admin')

@section('title') فرستادن پیام @endsection

@section('breadcrumbs')
<section class="content-header">
    <h1>
        داشبرد
        <small>کنترل پنل</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> خانه</a></li>
        <li class="active">داشبرد</li>
    </ol>
</section>
@endsection

    @push('styles')
       <link rel="stylesheet" href="/admin/bower_components/select2/dist/css/select2.min.css">
    @endpush

@section('content')

    <section class="content">


        <!-- /.row -->
        <div class="row">
            <section class="col-lg-12 col-md-12">

                @include('master.admin_message')

                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">افزودن محصول</h3>
                    </div>
                    
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" enctype="multipart/form-data" action="{{ route('admin.product.store') }}" method="post">

                        {{ csrf_field() }}

                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">نام</label>

                                <div class="col-sm-10">
                                    <input type="text" value="{{ old('name') }}" name="name" class="form-control" placeholder="نام">
                                </div>
                            </div>

                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">نام انگلیسی</label>

                                <div class="col-sm-10">
                                    <input type="text" value="{{ old('name_en') }}" name="name_en" class="form-control" placeholder="نام انگلیسی">
                                </div>
                            </div>

                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">عکس</label>

                                <div class="col-sm-10">
                                    <input type="file" name="image" >
                                </div>
                            </div>

                        </div>

                        <div class="box-body" id="show-user-lists">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">دسته بندی محصول</label>

                                <div class="col-sm-10">
                                    <select name="category_id" id="users-select" style="width: 100%" class="form-control select2" data-placeholder="Select a State">
                                        <option>انتخاب کنید</option>
                                        @foreach($productCategories as $productCategory)
                                            <option value="{{ $productCategory->id }}">{{ $productCategory->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">بها</label>

                                <div class="col-sm-10">
                                    <input type="text" value="{{ old('price') }}" name="price" class="form-control" placeholder="بها">
                                </div>
                            </div>

                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">تخفیف</label>

                                <div class="col-sm-10">
                                    <input type="text" value="{{ old('discount') }}" name="discount" class="form-control" placeholder="تخفیف">
                                </div>
                            </div>

                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">موجودی</label>

                                <div class="col-sm-10">
                                    <input type="text" value="{{ old('stock') }}" name="stock" class="form-control" placeholder="موجودی">
                                </div>
                            </div>

                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">بیشترین سفارش</label>

                                <div class="col-sm-10">
                                    <input type="text" value="{{ old('maximum_order') }}" name="maximum_order" class="form-control" placeholder="بیشترین سفارش">
                                </div>
                            </div>

                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">شناسه محصول</label>

                                <div class="col-sm-10">
                                    <input type="text" value="{{ old('product_code') }}" name="product_code" class="form-control" placeholder="شناسه محصول">
                                </div>
                            </div>

                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">خلاصه</label>

                                <div class="col-sm-10">
                                    <textarea id="summary" name="summary" rows="10" cols="80" style="visibility: hidden; display: none;">{{ old('summary') }}</textarea>
                                </div>
                            </div>

                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">نوشته</label>

                                <div class="col-sm-10">
                                    <textarea id="editor1" name="description" rows="10" cols="80" style="visibility: hidden; display: none;">{{ old('description') }}</textarea>
                                </div>
                            </div>

                        </div>

                        <div class="box-body" id="show-user-lists">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">وضعیت</label>

                                <div class="col-sm-10">
                                    <select name="status" class="form-control" data-placeholder="Select a State">
                                        <option value="1">فعال</option>
                                        <option value="2">غیرفعال</option>
                                    </select>
                                </div>
                            </div>

                        </div>

                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-info pull-right">فرستادن</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
            </section>
        </div>

    </section>
@endsection

@push('scripts')
    <script src="/admin/bower_components/ckeditor/ckeditor.js"></script>
    <script src="/admin/bower_components/select2/dist/js/select2.full.min.js"></script>

   <script>

       $(function () {

           CKEDITOR.replace('summary');

           CKEDITOR.replace('editor1');

           $('.select2').select2({
               dir:'rtl'
           });
       })

   </script>
    @endpush