@extends('master.admin')

@section('title') لیست دسته بندی محصول ها @endsection

@section('breadcrumbs')
    <section class="content-header">
        <h1>
            داشبرد
            <small>کنترل پنل</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> خانه</a></li>
            <li class="active">داشبرد</li>
        </ol>
    </section>
@endsection

@section('content')

    <div class="row">
        <section class="col-lg-12 col-md-12">

            @include('master.admin_message')

            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">لیست دسته بندی محصول ها</h3>
                </div>
                <!-- /.box-header -->

                <div class="box-body">

                    <table class="table table-bordered">
                        <tbody>

                        <tr>
                            <th style="width: 10px">#</th>
                            <th>نام</th>
                            <th>عملیات</th>
                        </tr>

                        @foreach($productCategories as $productCategory)
                            <tr>
                                <td>{{ $productCategory->id }}</td>
                                <td>{{ $productCategory->name }}</td>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <a href="{{ route('admin.productcategories.edit' , [$productCategory->id]) }}" class="btn btn-primary">ویرایش</a>
                                        </div>

                                        <div class="col-sm-6">
                                            <a href="{{ route('admin.productcategories.destroy' , [$productCategory->id]) }}" class="btn btn-danger">پاک کردن</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">
                    {{ $productCategories->links() }}
                    </ul>
                </div>
            </div>

        </section>
    </div>

@endsection

