@extends('master.admin')

@section('title') فرستادن پیام @endsection

@section('breadcrumbs')
<section class="content-header">
    <h1>
        داشبرد
        <small>کنترل پنل</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> خانه</a></li>
        <li class="active">داشبرد</li>
    </ol>
</section>
@endsection

    @push('styles')
       <link rel="stylesheet" href="/admin/bower_components/select2/dist/css/select2.min.css">
    @endpush

@section('content')

    <section class="content">


        <!-- /.row -->
        <div class="row">
            <section class="col-lg-12 col-md-12">

                @include('master.admin_message')

                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">افزودن دسته بندی محصول</h3>
                    </div>
                    
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" action="{{ route('admin.productcategories.store') }}" method="post">

                        {{ csrf_field() }}

                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">عنوان</label>

                                <div class="col-sm-10">
                                    <input type="text" name="name" class="form-control" placeholder="عنوان">
                                </div>
                            </div>

                        </div>

                        <div class="box-body" id="show-user-lists">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">دسته بندی محصول</label>

                                <div class="col-sm-10">
                                    <select name="category_id" id="users-select" style="width: 100%" class="form-control select2" data-placeholder="Select a State">
                                        <option>انتخاب کنید</option>
                                        <option value="0">دسته بندی پدر</option>
                                        @foreach($productCategories as $productCategory)
                                            <option value="{{ $productCategory->id }}">{{ $productCategory->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                        </div>

                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-info pull-right">فرستادن</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
            </section>
        </div>

    </section>
@endsection

@push('scripts')
    <script src="/admin/bower_components/select2/dist/js/select2.full.min.js"></script>

   <script>

       $(function () {
           $('.select2').select2({
               dir: 'rtl'
           });
       })

   </script>
    @endpush