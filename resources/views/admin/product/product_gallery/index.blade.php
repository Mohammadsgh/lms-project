@extends('master.admin')

@section('title') لیست گالری محصول ها @endsection

@section('breadcrumbs')
    <section class="content-header">
        <h1>
            داشبرد
            <small>کنترل پنل</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> خانه</a></li>
            <li class="active">داشبرد</li>
        </ol>
    </section>
@endsection

@section('content')

    <div class="row">
        <section class="col-lg-12 col-md-12">

            @include('master.admin_message')

            <div class="box box-info">
                <div class="box-header with-border">
                    <a href="{{ route('admin.productgalleries.create') }}" class="btn btn-primary">افزودن گالری</a>
                    <h3 class="box-title">لیست گالری محصول ها</h3>
                </div>
                <!-- /.box-header -->

                <div class="box-body">

                    <table class="table table-bordered">
                        <tbody>

                        <tr>
                            <th style="width: 10px">#</th>
                            <th>عنوان</th>
                            <th>عکس کوچک</th>
                            <th>جایگزین</th>
                            <th>عملیات</th>
                        </tr>

                        @foreach($productGalleries as $productGallery)
                            <tr>
                                <td>{{ $productGallery->id }}</td>
                                <td>{{ $productGallery->title }}</td>
                                <td> <img src="/{{ $productGallery->thumbnail_image }}" /></td>
                                <td>{{ $productGallery->alt }}</td>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <a href="{{ route('admin.productgalleries.edit' , [$productGallery->id]) }}" class="btn btn-primary">ویرایش</a>
                                        </div>

                                        <div class="col-sm-6">
                                            <a href="{{ route('admin.productgalleries.destroy' , [$productGallery->id]) }}" class="btn btn-danger">پاک کردن</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">
                        {{ $productGalleries->links() }}
                    </ul>
                </div>
            </div>

        </section>
    </div>

@endsection
