@extends('master.admin')

@section('title') ویرایش گالری محصول @endsection

@section('breadcrumbs')
    <section class="content-header">
        <h1>
            داشبرد
            <small>کنترل پنل</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> خانه</a></li>
            <li class="active">داشبرد</li>
        </ol>
    </section>
@endsection

@push('styles')
    <link rel="stylesheet" href="/admin/bower_components/select2/dist/css/select2.min.css">
@endpush

@section('content')

    <section class="content">


        <!-- /.row -->
        <div class="row">
            <section class="col-lg-12 col-md-12">

                @include('master.admin_message')

                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">ویرایش گالری بندی محصول</h3>
                    </div>

                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" enctype="multipart/form-data" action="{{ route('admin.productgalleries.update' , [$productGallery->id]) }}" method="post">

                        <input type="hidden" name="_method" value="PUT">

                        {{ csrf_field() }}

                        <div id="show-galleries">
                            <div class="row">
                                <div class="col-sm-4">
                                    <input type="text" value="{{ $productGallery->title }}" name="title" class="form-control" placeholder="عنوان">
                                </div>
                                <div class="col-sm-4">
                                    <input type="file" name="image">
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" value="{{ $productGallery->alt }}" name="alt" class="form-control" placeholder="جایگزین">
                                </div>
                            </div>
                        </div>

                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-info pull-right">فرستادن</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
            </section>
        </div>

    </section>
@endsection

@push('scripts')
    <script src="/admin/bower_components/select2/dist/js/select2.full.min.js"></script>

    <script>

        $(document).ready(function () {
            $('#add-product-gallery').click(function (e) {

                e.preventDefault();

                $('#show-galleries').append('<div class="row"> <div class="col-sm-4"> <input type="text" name="title[]" class="form-control" placeholder="عنوان"> </div> <div class="col-sm-4"> <input type="file" name="image[]"> </div> <div class="col-sm-4"> <input type="text" name="alt[]" class="form-control" placeholder="جایگزین"> </div> </div>');

            });
        });

        $(function () {
            $('.select2').select2({
                dir: 'rtl'
            });
        })

    </script>
@endpush