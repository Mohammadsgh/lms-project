@extends('master.admin')

@section('title') لیست محصول ها @endsection

@section('breadcrumbs')
    <section class="content-header">
        <h1>
            داشبرد
            <small>کنترل پنل</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> خانه</a></li>
            <li class="active">داشبرد</li>
        </ol>
    </section>
@endsection

@section('content')

    <div class="row">
        <section class="col-lg-12 col-md-12">

            @include('master.admin_message')

            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">لیست محصول ها</h3>
                </div>
                <!-- /.box-header -->

                <div class="box-body">

                    <table class="table table-bordered">
                        <tbody>

                        <tr>
                            <th style="width: 10px">#</th>
                            <th>نام</th>
                            <th>دسته بندی</th>
                            <th>بها</th>
                            <th>وضعیت</th>
                            <th>عملیات</th>
                        </tr>

                        @foreach($products as $product)
                            <tr>
                                <td>{{ $product->id }}</td>
                                <td>{{ $product->name }}</td>
                                <td>{{ $product->category->name }}</td>
                                <td>{{ $product->price }}</td>
                                <td>
                                    @if( $product->status == 1 )
                                        فعال
                                    @else
                                        غیرفعال
                                    @endif
                                </td>

                                <td>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <a href="{{ route('admin.product.edit' , [$product->id]) }}" class="btn btn-primary">ویرایش</a>
                                        </div>

                                        <div class="col-sm-2">
                                            <a href="{{ route('admin.product.show' , [$product->id]) }}" class="btn btn-success">نمایش</a>
                                        </div>

                                        <div class="col-sm-2">
                                            <a href="{{ route('admin.product.destroy' , [$product->id]) }}" class="btn btn-danger">پاک کردن</a>
                                        </div>

                                        <div class="col-sm-2">
                                            <a href="{{ route('admin.productgalleries.index' , [$product->id]) }}" class="btn btn-success">گالری</a>
                                        </div>

                                        <div class="col-sm-2">
                                            <a href="{{ route('admin.productcomments.index' , [$product->id]) }}" class="btn btn-primary">دیدگاه</a>
                                        </div>

                                        <div class="col-sm-2">
                                            <a href="{{ route('admin.product.destroy' , [$product->id]) }}" class="btn btn-danger">پاک کردن</a>
                                        </div>

                                    </div>
                                </td>

                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">
                    {{ $products->links() }}
                    </ul>
                </div>
            </div>

        </section>
    </div>

@endsection

