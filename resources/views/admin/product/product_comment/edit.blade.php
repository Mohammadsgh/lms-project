@extends('master.admin')

@section('title') ویرایش نقش @endsection

@section('breadcrumbs')
    <section class="content-header">
        <h1>
            داشبرد
            <small>کنترل پنل</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> خانه</a></li>
            <li class="active">داشبرد</li>
        </ol>
    </section>
@endsection

@push('styles')
    <link rel="stylesheet" href="/admin/bower_components/select2/dist/css/select2.min.css">
@endpush

@section('content')

    <section class="content">


        <!-- /.row -->
        <div class="row">
            <section class="col-lg-12 col-md-12">

                @include('master.admin_message')

                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">ویرایش نقش</h3>
                    </div>


                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">نام</label>

                                <div class="col-sm-10">
                                    <input type="text" value="{{ $productComment->firstname }}" name="name" class="form-control"
                                           placeholder="نام">
                                </div>
                            </div>

                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">نام خانوادگی</label>

                                <div class="col-sm-10">
                                    <input type="text" value="{{ $productComment->lastname }}" name="price" class="form-control"
                                           placeholder="بها">
                                </div>
                            </div>

                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">ایمیل</label>

                                <div class="col-sm-10">
                                    <input type="text" value="{{ $productComment->email }}" name="email"
                                           class="form-control" placeholder="تخفیف">
                                </div>
                            </div>

                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">پیام</label>

                                <div class="col-sm-10">
                                    <textarea name="stock" class="form-control">{{ $productComment->description }}</textarea>
                                </div>
                            </div>

                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">بیشترین سفارش</label>

                                <div class="col-sm-10">
                                    <textarea name="stock" class="form-control">
                                        @if($productComment->status == 1)
                                            فعال
                                            @else
                                            غیرفعال
                                        @endif
                                    </textarea>
                                </div>
                            </div>

                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">تاریخ ساخت</label>

                                <div class="col-sm-10">
                                    <input type="text" value="{{ $productComment->created_at }}" name="product_code"
                                           class="form-control" placeholder="شناسه محصول">
                                </div>
                            </div>

                        </div>

                </div>
            </section>
        </div>

    </section>
@endsection

