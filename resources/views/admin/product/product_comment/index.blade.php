@extends('master.admin')

@section('title') لیست دیدگاه محصول ها @endsection

@section('breadcrumbs')
    <section class="content-header">
        <h1>
            داشبرد
            <small>کنترل پنل</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> خانه</a></li>
            <li class="active">داشبرد</li>
        </ol>
    </section>
@endsection

@section('content')

    <div class="row">
        <section class="col-lg-12 col-md-12">

            @include('master.admin_message')

            <div class="box box-info">

                <div class="box-body">

                    <table class="table table-bordered">
                        <tbody>

                        <tr>
                            <th style="width: 10px">#</th>
                            <th>نام</th>
                            <th>نام خانوادگی</th>
                            <th>ایمیل</th>
                            <th>تاریخ ساختن</th>
                            <th>وضعیت</th>
                            <th>عملیات</th>
                        </tr>

                        @foreach($productComments as $productComment)
                            <tr>
                                <td>{{ $productComment->id }}</td>
                                <td>{{ $productComment->firstname }}</td>
                                <td>{{ $productComment->lastname }}</td>
                                <td>{{ $productComment->email }}</td>
                                <td>{{ $productComment->created_at }}</td>
                                <td>
                                    @if($productComment->status == 1)

                                        فعال
                                    @else
                                        غیرفعال
                                    @endif
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <a href="{{ route('admin.productcomments.editstatus' , [$productComment->id , 1]) }}"
                                               class="btn btn-primary">تایید</a>

                                            <a href="{{ route('admin.productcomments.editstatus' , [$productComment->id , 0]) }}"
                                               class="btn btn-danger">رد</a>

                                        </div>

                                        <div class="col-sm-4">
                                            <a href="{{ route('admin.productcomments.show' , [$productComment->id]) }}"
                                               class="btn btn-success">نمایش پیام</a>
                                        </div>

                                        <div class="col-sm-4">
                                            <a href="{{ route('admin.productcomments.destroy' , [$productComment->id]) }}"
                                               class="btn btn-danger">پاک کردن</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">
                        {{ $productComments->links() }}
                    </ul>
                </div>
            </div>

        </section>
    </div>

@endsection

