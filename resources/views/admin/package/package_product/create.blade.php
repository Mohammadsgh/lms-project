@extends('master.admin')

@section('title') افزودن محصول به پکیج @endsection

@section('breadcrumbs')
    <section class="content-header">
        <h1>
            داشبرد
            <small>کنترل پنل</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> خانه</a></li>
            <li class="active">داشبرد</li>
        </ol>
    </section>
@endsection

@push('styles')
    <link rel="stylesheet" href="/admin/bower_components/select2/dist/css/select2.min.css">
@endpush

@section('content')

    <section class="content">


        <!-- /.row -->
        <div class="row">
            <section class="col-lg-12 col-md-12">

                @include('master.admin_message')

                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">افزودن محصول به پکیج</h3>
                    </div>

                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" action="{{ route('admin.packageproduct.store') }}" method="post">

                        {{ csrf_field() }}

                        <div class="box-body" id="show-user-lists">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">پکیج</label>

                                <div class="col-sm-10">
                                    <select name="package" class="package form-control"></select>
                                </div>
                            </div>

                        </div>

                        <div class="box-body" id="show-user-lists">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">محصول ها</label>

                                <div class="col-sm-10">
                                    <select name="products[]" multiple="multiple" class="product form-control"></select>
                                </div>
                            </div>

                        </div>

                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-info pull-right">فرستادن</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
            </section>
        </div>

    </section>
@endsection

@push('scripts')
    <script src="/admin/bower_components/select2/dist/js/select2.full.min.js"></script>

    <script>

        // $(function () {
        $(document).ready(function () {

            $('.product').select2({
                dir: 'rtl',
                ajax: {
                    url: '{{ route('admin.packageproduct.products') }}',
                    dataType: 'json',
                    delay: 250,
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                }
            });

            $('.package').select2({
                dir: 'rtl',
                tags: true,
                ajax: {
                    url: '{{ route('admin.packageproduct.packages') }}',
                    dataType: 'json',
                    delay: 250,
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                }
            });

        });
        // })

    </script>
@endpush