@extends('master.admin')

@section('title') لیست محصول پکیج ها @endsection

@section('breadcrumbs')
    <section class="content-header">
        <h1>
            داشبرد
            <small>کنترل پنل</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> خانه</a></li>
            <li class="active">داشبرد</li>
        </ol>
    </section>
@endsection

@section('content')

    <div class="row">
        <section class="col-lg-12 col-md-12">

            @include('master.admin_message')

            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">لیست محصول پکیج ها</h3> /
                    <h3 class="box-title">{{ $package->name }}</h3>
                </div>
                <!-- /.box-header -->

                <div class="box-body">

                    <table class="table table-bordered">
                        <tbody>

                        <tr>
                            <th style="width: 10px">#</th>
                            <th>نام</th>
                            <th>شناسه محصول</th>
                            <th>بها</th>
                            <th>عملیات</th>
                        </tr>

                        @foreach($products as $product)
                            <tr>
                                <td>{{ $product->id }}</td>
                                <td>{{ $product->name }}</td>
                                <td>{{ $product->product_code }}</td>
                                <td>{{ $product->price }}</td>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <a href="{{ route('admin.packageproduct.destroy' , [$product->id]) }}"
                                               class="btn btn-danger">پاک کردن</a>
                                        </div>
                                    </div>
                                </td>

                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">
                        {{ $products->links() }}
                    </ul>
                </div>
            </div>

        </section>
    </div>

@endsection

