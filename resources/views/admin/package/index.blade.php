@extends('master.admin')

@section('title') لیست پکیج ها @endsection

@section('breadcrumbs')
    <section class="content-header">
        <h1>
            داشبرد
            <small>کنترل پنل</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> خانه</a></li>
            <li class="active">داشبرد</li>
        </ol>
    </section>
@endsection

@section('content')

    <div class="row">
        <section class="col-lg-12 col-md-12">

            @include('master.admin_message')

            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">لیست پکیج ها</h3>
                </div>
                <!-- /.box-header -->

                <div class="box-body">

                    <table class="table table-bordered">
                        <tbody>

                        <tr>
                            <th style="width: 10px">#</th>
                            <th>نام</th>
                            <th>بها</th>
                            <th>شناسه پکیج</th>
                            <th>وضعیت</th>
                            <th>عملیات</th>
                        </tr>

                        @foreach($packages as $package)
                            <tr>
                                <td>{{ $package->id }}</td>
                                <td>{{ $package->name }}</td>
                                <td>{{ $package->price }}</td>
                                <td>{{ $package->package_code }}</td>
                                <td>
                                    @if( $package->status == 1 )
                                        فعال
                                    @else
                                        غیرفعال
                                    @endif
                                </td>

                                <td>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <a href="{{ route('admin.package.edit' , [$package->id]) }}" class="btn btn-primary">ویرایش</a>
                                        </div>

                                        <div class="col-sm-3">
                                            <a href="{{ route('admin.package.show' , [$package->id]) }}" class="btn btn-success">نمایش</a>
                                        </div>

                                        <div class="col-sm-3">
                                            <a href="{{ route('admin.packageproduct.list' , [$package->id]) }}" class="btn btn-success">نمایش محصول ها</a>
                                        </div>

                                        <div class="col-sm-3">
                                            <a href="{{ route('admin.package.destroy' , [$package->id]) }}" class="btn btn-danger">پاک کردن</a>
                                        </div>

                                    </div>
                                </td>

                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">
                    {{ $packages->links() }}
                    </ul>
                </div>
            </div>

        </section>
    </div>

@endsection

