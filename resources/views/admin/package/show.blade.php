@extends('master.admin')

@section('title') نمایش پکیج @endsection

@section('breadcrumbs')
    <section class="content-header">
        <h1>
            داشبرد
            <small>کنترل پنل</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> خانه</a></li>
            <li class="active">داشبرد</li>
        </ol>
    </section>
@endsection

@push('styles')
    <link rel="stylesheet" href="/admin/bower_components/select2/dist/css/select2.min.css">
@endpush

@section('content')

    <section class="content">


        <!-- /.row -->
        <div class="row">
            <section class="col-lg-12 col-md-12">

                @include('master.admin_message')

                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">نمایش پکیج</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->

                    {{ csrf_field() }}

                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">نام</label>

                            <div class="col-sm-10">
                                <input type="text" value="{{ $package->name }}" name="name" class="form-control"
                                       placeholder="نام">
                            </div>
                        </div>

                    </div>

                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">نام انگلیسی</label>

                            <div class="col-sm-10">
                                <input type="text" value="{{ $package->name_en }}" name="name_en"
                                       class="form-control" placeholder="نام انگلیسی">
                            </div>
                        </div>

                    </div>

                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">عکس</label>

                            <div class="col-sm-10">
                                @if(! empty($package->image))
                                    <img src="/{{ $package->image }}" width="100">
                                @else
                                    ندارد
                                @endif
                            </div>
                        </div>

                    </div>

                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">بها</label>

                            <div class="col-sm-10">
                                <input type="text" value="{{ $package->price }}" name="price" class="form-control"
                                       placeholder="بها">
                            </div>
                        </div>

                    </div>

                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">تخفیف</label>

                            <div class="col-sm-10">
                                <input type="text" value="{{ $package->discount }}" name="discount"
                                       class="form-control" placeholder="تخفیف">
                            </div>
                        </div>

                    </div>

                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">موجودی</label>

                            <div class="col-sm-10">
                                <input type="text" value="{{ $package->stock }}" name="stock" class="form-control"
                                       placeholder="موجودی">
                            </div>
                        </div>

                    </div>

                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">بیشترین سفارش</label>

                            <div class="col-sm-10">
                                <input type="text" value="{{ $package->maximum_order }}" name="maximum_order"
                                       class="form-control" placeholder="بیشترین سفارش">
                            </div>
                        </div>

                    </div>

                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">شناسه پکیج</label>

                            <div class="col-sm-10">
                                <input type="text" value="{{ $package->package_code }}" name="package_code"
                                       class="form-control" placeholder="شناسه پکیج">
                            </div>
                        </div>

                    </div>

                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">خلاصه</label>

                            <div class="col-sm-10">
                                    <textarea id="summary" name="summary" rows="10" cols="80"
                                              style="visibility: hidden; display: none;">{{ $package->summary }}</textarea>
                            </div>
                        </div>

                    </div>

                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">نوشته</label>

                            <div class="col-sm-10">
                                    <textarea id="editor1" name="description" rows="10" cols="80"
                                              style="visibility: hidden; display: none;">{{ $package->description }}</textarea>
                            </div>
                        </div>

                    </div>

                    <div class="box-body" id="show-user-lists">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">وضعیت</label>

                            <div class="col-sm-10">
                                <select name="status" class="form-control" data-placeholder="Select a State">
                                    @if($package->status == 1)
                                        <option value="1">فعال</option>
                                    @else
                                        <option value="2">غیرفعال</option>
                                    @endif
                                </select>
                            </div>
                        </div>

                    </div>

                </div>
            </section>
        </div>

    </section>
@endsection

@push('scripts')
    <script src="/admin/bower_components/ckeditor/ckeditor.js"></script>

    <script>

        $(function () {

            CKEDITOR.replace('summary');

            CKEDITOR.replace('editor1');

        })

    </script>
@endpush