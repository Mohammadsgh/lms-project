<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryFeatureItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_feature_items', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('category_id')->index();
            $table->unsignedInteger('feature_id')->index();
            $table->unsignedInteger('feature_value_id')->index();
            $table->boolean('status');

            $table->foreign('category_id')
                ->references('id')->on('product_categories')
                ->onDelete('cascade');

            $table->foreign('feature_id')
                ->references('id')->on('category_features')
                ->onDelete('cascade');

            $table->foreign('feature_value_id')
                ->references('id')->on('category_feature_values')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_feature_items');
    }
}
