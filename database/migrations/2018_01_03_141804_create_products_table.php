<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->index();
            $table->unsignedInteger('category_id')->index();
            $table->string('name');
            $table->string('name_en')->nullable();
            $table->string('image')->nullable();
            $table->unsignedInteger('price')->nullable()->comment('قیمت');
            $table->unsignedMediumInteger('discount')->default(0)->comment('تخفیف');
            $table->unsignedSmallInteger('stock')->nullable()->comment('موجودی');
            $table->unsignedTinyInteger('maximum_order')->nullable()->index()->comment('بیشترین سفارش');
            $table->string('product_code', 25)->nullable()->comment('شناسه محصول');
            $table->text('summary');
            $table->text('description');
            $table->boolean('status');
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');

            $table->foreign('category_id')
                ->references('id')->on('product_categories')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
