<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryFeatureValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_feature_values', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('feature_id')->index();
            $table->string('name');
            $table->string('image')->nullable();
            $table->text('description')->nullable();

            $table->foreign('feature_id')
                ->references('id')->on('category_features')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_feature_values');
    }
}
