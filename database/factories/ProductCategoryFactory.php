<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\V1\Product\ProductCategory::class , function (Faker $faker) {

    return [
        'parent_id' => 0,
        'name' => $faker->name,
    ];
});
