<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\V1\Product\Product::class, function (Faker $faker) {

    return [
        'user_id' => rand(1 , 9),
        'category_id' => rand(1 , 9),
        'name' => $faker->title,
        'name_en' => $faker->title,
        'image' => 'test.jpg',
        'price' => rand(1000 , 1000000),
        'discount' => rand(10 , 100),
        'stock' => rand(10 , 100),
        'maximum_order' => rand(10 , 100),
        'product_code' => rand(1000000 , 999999),
        'summary' => $faker->text(),
        'description' => $faker->text(),
        'status' => 1
    ];
});


$factory->define(App\Models\V1\Product\ProductComment::class, function (Faker $faker) {

    return [
        'product_id' => rand(1 , 50),
        'parent_id' => 0,
        'firstname' => $faker->firstName,
        'lastname' => $faker->lastName,
        'email' => $faker->email,
        'description' => $faker->text,
        'status' => rand(0 , 1),
    ];
});