<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        \App\Models\V1\Product\ProductCategory::truncate();
        \App\Models\V1\Product\Product::truncate();

        factory(App\Models\V1\Product\ProductCategory::class, 50)->create()->each(function ($u) {
            $u->product()->save(factory(App\Models\V1\Product\Product::class)->make());
        });

        factory(App\Models\V1\Product\ProductComment::class, 50)->create();
    }
}
