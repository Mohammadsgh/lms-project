<?php

use Illuminate\Database\Seeder;

class PackageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
//        \App\Models\V1\Package\PackageCategory::truncate();
        \App\Models\V1\Package\Package::truncate();

        factory(App\Models\V1\Package\PackageCategory::class, 50)->create()->each(function ($u) {
            $u->product()->save(factory(App\Models\V1\Package\Package::class)->make());
        });

        factory(App\Models\V1\Package\PackageComment::class, 50)->create();
    }
}
