<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    protected $adminNamespace = 'App\Http\Controllers\Admin';

    protected $profileNamespace = 'App\Http\Controllers\Profile';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();
        $this->mapWebRoutes();
        $this->mapAdminRoutes();
        $this->mapProfileRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }

    protected function mapAdminRoutes()
    {
        Route::group([
            'middleware' => 'web',
            'prefix' => 'admin',
            'namespace' => $this->adminNamespace,
            'as' => 'admin.'
        ], function () {

            $adminRoutes = scandir(base_path('routes/admin'));
            unset($adminRoutes[0] , $adminRoutes[1]);

            foreach ($adminRoutes as $adminRoute)
            {
                require base_path('routes/admin/' . $adminRoute);
            }

        });
    }

    protected function mapProfileRoutes()
    {
        Route::group([
            'middleware' => 'web',
            'prefix' => 'profile',
            'namespace' => $this->profileNamespace,
        ], function () {

            $adminRoutes = scandir(base_path('routes/profile'));
            unset($adminRoutes[0] , $adminRoutes[1]);

            foreach ($adminRoutes as $adminRoute)
            {
                require base_path('routes/profile/' . $adminRoute);
            }

        });
    }

}
