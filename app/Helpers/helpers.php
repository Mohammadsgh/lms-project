<?php


function gregorianToJalali($date , $format = '%B %d، %Y')
{
    return \Morilog\Jalali\jDate::forge($date)->format($format);
}

function checkRole(array $roles = ['admin'])
{
    $checkRole = Illuminate\Support\Facades\Auth::user()->hasRole($roles);
    return $checkRole !== true ? abort(401) : null;
}