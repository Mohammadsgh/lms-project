<?php

namespace App\Helpers;

use Intervention\Image\Facades\Image;

class ImageUpload
{

    protected static $instance;
    private static $files;
    private static $path;
    private $request;
    private $result;

    public static function getInstance()
    {
        if (is_null(static::$instance)) {
            static::$instance = new static;
        }

        return static::$instance;
    }

    public function files($request, $file)
    {
        $this->request = $request;

        self::$files = $file;

        return $this;
    }

    public function store($path, $multiple = false)
    {
        self::$path = $path;

        if ($multiple === false) {
            foreach (self::$files as $file) {

                $image = $this->request->file($file)->store($path);
                $thumbs = explode('/', $image);

                $this->result[] = [
                    'path' => $image,
                    'thumbs' => $path . '/thumbs/' . end($thumbs),
                    'mime' => $this->request->file($file)->getMimeType(),
                    'size' => intval($this->request->file($file)->getClientSize() / 1000),
                    'width' => getimagesize($this->request->file($file))[0],
                    'height' => getimagesize($this->request->file($file))[1],
                ];
            }
        }

        $this->multiUpload($path);

        return $this;
    }

    private function multiUpload($path)
    {
        foreach (self::$files as $file) {

            foreach ($this->request->file($file) as $fileInput) {
                $image = $fileInput->store($path);
                $thumbs = explode('/', $image);

                $this->result[] = [
                    'path' => $image,
                    'thumbs' => $path . '/thumbs/' . end($thumbs),
                    'mime' => $fileInput->getMimeType(),
                    'size' => intval($fileInput->getClientSize() / 1000),
                    'width' => getimagesize($fileInput)[0],
                    'height' => getimagesize($fileInput)[1],
                ];
            }
        }
    }

    public function resize($type, $width, $height)
    {

        foreach ($this->result as $result) {
            $w = $width * 10;
            if ($w <= 0) $w = null;
            $h = $height * 10;
            if ($h <= 0) $h = null;
            if ($w > 2000 || $h > 1500)
                abort(404);
            if (!in_array($type, ['_', '-']))
                return abort(404);

            if (file_exists(public_path($result['path'])))
                $img = Image::make(public_path($result['path']));
            else {
                if (($image404 = config('image-fit.image_404')) && file_exists(public_path($image404))) {
                    $img = Image::make($image404);
                } elseif (file_exists(public_path('vendor/image-fit/404.jpg'))) {
                    $img = Image::make(public_path('vendor/image-fit/404.jpg'));
                } else
                    abort(404);
            }
            switch ($type) {
                case '_':
                    if ($w == null || $h == null)
                        return abort(404);
                    else {
                        $img->fit($w, $h);
                    }
                    break;
                case '-':
                    if ($w != null && $h == null)
                        $img->widen($w);
                    elseif ($w == null && $h != null)
                        $img->heighten($h);
                    elseif ($w != null && $h != null) {
                        $img->resize($w, $h, function ($constraint) {
                            $constraint->aspectRatio();
                            #$constraint->upsize();
                        });
                    }
                    break;
            }

            $image = $result['path'];
            $thumbs = explode('/', $image);

            $img->save(public_path(self::$path . '/' . end($thumbs)));

        }

        return $this;
    }

    public function thumb($type, $width, $height)
    {

        foreach ($this->result as $result) {
            $w = $width * 10;
            if ($w <= 0) $w = null;
            $h = $height * 10;
            if ($h <= 0) $h = null;
            if ($w > 2000 || $h > 1500)
                abort(404);
            if (!in_array($type, ['_', '-']))
                return abort(404);

            if (file_exists(public_path($result['path'])))
                $img = Image::make(public_path($result['path']));
            else {
                if (($image404 = config('image-fit.image_404')) && file_exists(public_path($image404))) {
                    $img = Image::make($image404);
                } elseif (file_exists(public_path('vendor/image-fit/404.jpg'))) {
                    $img = Image::make(public_path('vendor/image-fit/404.jpg'));
                } else
                    abort(404);
            }
            switch ($type) {
                case '_':
                    if ($w == null || $h == null)
                        return abort(404);
                    else {
                        $img->fit($w, $h);
                    }
                    break;
                case '-':
                    if ($w != null && $h == null)
                        $img->widen($w);
                    elseif ($w == null && $h != null)
                        $img->heighten($h);
                    elseif ($w != null && $h != null) {
                        $img->resize($w, $h, function ($constraint) {
                            $constraint->aspectRatio();
                            #$constraint->upsize();
                        });
                    }
                    break;
            }

            $image = $result['path'];
            $thumbs = explode('/', $image);

            @mkdir(public_path(self::$path) . '/thumbs', 0755, true);
            $img->save(public_path(self::$path . '/thumbs/' . end($thumbs)));

        }

        return $this;
    }

    public function get()
    {
        return $this->result;

    }

}