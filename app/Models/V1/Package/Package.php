<?php

namespace App\Models\V1\Package;

use App\Models\V1\Product\Product;
use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $fillable = ['user_id','name_en', 'discount' , 'category_id','package_code','stock' , 'price' , 'maximum_order' , 'summary' , 'name' , 'image' , 'description' , 'status'];

    public function products()
    {
        return $this->belongsToMany(Product::class, 'package_products', 'package_id', 'product_id');
    }
}
