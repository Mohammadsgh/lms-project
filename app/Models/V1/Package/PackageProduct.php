<?php

namespace App\Models\V1\Package;

use Illuminate\Database\Eloquent\Model;

class PackageProduct extends Model
{

    public $timestamps = false;
    protected $fillable = ['package_id', 'product_id', 'status'];


    public function products()
    {
        return $this->belongsToMany(Package::class, 'package_products', 'product_id', 'id');
    }
}
