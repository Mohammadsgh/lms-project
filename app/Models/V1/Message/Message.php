<?php

namespace App\Models\V1\Message;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = ['title' , 'type' , 'status'];

    /**
     * get Message Owner User
     */
    public function message_user()
    {
        return $this->belongsToMany(User::class , 'message_users' , 'message_id' , 'target_id')->select('username');
    }

    public function message_category()
    {
        return $this->belongsToMany(Message::class , 'message_users' , 'message_id' , 'target_id')->select('title');
    }

    public function message_content()
    {
        return $this->hasMany(MessageContent::class , 'message_id')->select('content');
    }
}
