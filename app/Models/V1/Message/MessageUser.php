<?php

namespace App\Models\V1\Message;

use Illuminate\Database\Eloquent\Model;

class MessageUser extends Model
{
    protected $fillable = ['message_id' , 'user_id' , 'target_id' , 'type'];
    public $timestamps = false;
}
