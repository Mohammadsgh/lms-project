<?php

namespace App\Models\V1\Message;

use Illuminate\Database\Eloquent\Model;

class MessageContent extends Model
{
    protected $fillable = ['message_id' , 'content'];
    public $timestamps = false;
}
