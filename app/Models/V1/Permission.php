<?php

namespace App\Models\V1;

use Laratrust\Models\LaratrustPermission;

class Permission extends LaratrustPermission
{
    protected $fillable = ['name' , 'display_name' , 'description'];
}
