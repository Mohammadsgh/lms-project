<?php

namespace App\Models\V1;

use Laratrust\Models\LaratrustRole;

class Role extends LaratrustRole
{

    protected $fillable = ['name' , 'display_name' , 'description'];
}
