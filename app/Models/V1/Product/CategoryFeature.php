<?php

namespace App\Models\V1\Product;

use Illuminate\Database\Eloquent\Model;

class CategoryFeature extends Model
{
    protected $fillable = ['name' , 'image' , 'description'];
    public $timestamps = false;


    /*
     * Relation between feature value Section
     */

    public function feature_value()
    {
        return $this->hasMany(CategoryFeatureValue::class , 'feature_id');
    }
}
