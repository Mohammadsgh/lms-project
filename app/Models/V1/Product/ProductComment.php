<?php

namespace App\Models\V1\Product;

use Illuminate\Database\Eloquent\Model;

class ProductComment extends Model
{
    protected $fillable = ['product_id' , 'parent_id' , 'firstname' , 'lastname' , 'email' , 'description' , 'status'];

    /*
     * Create mutators for dates
     */

    public function getCreatedAtAttribute($value)
    {
        return gregorianToJalali($value);
    }

}
