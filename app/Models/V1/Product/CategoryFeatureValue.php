<?php

namespace App\Models\V1\Product;

use Illuminate\Database\Eloquent\Model;

class CategoryFeatureValue extends Model
{
    protected $fillable = ['name' , 'image' , 'description' , 'feature_id'];
    public $timestamps = false;

    public function feature()
    {
        return $this->belongsTo(CategoryFeature::class);
    }
}
