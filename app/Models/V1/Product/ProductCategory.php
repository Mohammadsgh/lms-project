<?php

namespace App\Models\V1\Product;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    protected $fillable = ['parent_id', 'name'];
    public $timestamps = false;

    public function product()
    {
        return $this->hasMany(Product::class, 'category_id');
    }

    public function category_features()
    {
        return $this->belongsToMany(CategoryFeature::class, 'category_feature_items', 'category_id', 'feature_id');
    }

    public function category_feature_values()
    {
        return $this->belongsToMany(CategoryFeature::class, 'category_feature_items', 'category_id', 'feature_value_id');
    }

}
