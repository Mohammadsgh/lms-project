<?php

namespace App\Models\V1\Product;

use Illuminate\Database\Eloquent\Model;

class ProductGallery extends Model
{
    protected $fillable = ['product_id' , 'thumbnail_image' , 'image' , 'alt'];
    public $timestamps = false;
}
