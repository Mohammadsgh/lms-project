<?php

namespace App\Models\V1\Product;

use App\Models\V1\Package\Package;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['user_id', 'discount', 'name_en', 'stock', 'category_id', 'price', 'maximum_order', 'summary', 'name', 'image', 'description', 'status'];

    public function category()
    {
        return $this->belongsTo(ProductCategory::class)->select('name');
    }

    public function packages()
    {
        return $this->belongsToMany(Package::class, 'package_products', 'product_id', 'package_id');
    }
}
