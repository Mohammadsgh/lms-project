<?php

namespace App\Models\V1\Product;

use Illuminate\Database\Eloquent\Model;

class CategoryFeatureItem extends Model
{
    protected $fillable = ['category_id' , 'feature_id', 'feature_value_id' , 'status'];
    public $timestamps = false;
}
