<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id' => 'required|numeric',
            'name' => 'required|string|max:255',
            'name_en' => 'nullable|string|max:255',
            'image' => 'nullable|image|mimes:jpg,jpeg,png,bmp',
            'price' => 'required|numeric',
            'discount' => 'nullable|numeric',
            'stock' => 'nullable|numeric',
            'maximum_order' => 'nullable|numeric',
            'product_code' => 'nullable',
            'summary' => 'required',
            'description' => 'required',
            'status' => 'required',
        ];
    }
}
