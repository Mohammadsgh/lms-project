<?php

namespace App\Http\Controllers\Admin\Role;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\V1\Permission;

class PermissionController extends Controller
{
    /**
     * Display a listing of the roles.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permissions = Permission::paginate(10);
        return view('admin.role.permission.index', compact('permissions'));
    }

    /**
     * Show the form for creating a new role.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.role.permission.add');
    }

    /**
     * Store a newly created role in role table.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'display_name' => 'nullable',
            'description' => 'nullable',
        ]);

        try {

            Permission::create($request->all());

        } catch (\Exception $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }

        return redirect()->route('permission.index')->with('message', 'دسترسی ساخته شد');

    }

    /**
     * Show the form for editing the role.
     *
     * @param Permission $permission
     * @return \Illuminate\Http\Response
     */
    public function edit(Permission $permission)
    {
        return view('admin.role.permission.edit', compact('permission'));
    }

    /**
     * Update the specified resource in role.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return void
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'name' => 'required',
            'display_name' => 'nullable',
            'description' => 'nullable',
        ]);

        Permission::find($id)->update($request->all());

        return redirect()->route('permission.index')->with('message', 'نقش به روز شد');

    }

    /**
     * Remove the specified resource from role.
     *
     * @param  int $id
     * @return void
     */
    public function destroy($id)
    {
        Permission::find($id)->delete();

        return redirect()->route('permission.index')->with('message', 'دسترسی پاک شد');
    }
}
