<?php

namespace App\Http\Controllers\Admin\Role;

use App\Models\V1\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mockery\Exception;

class RoleController extends Controller
{
    /**
     * Display a listing of the roles.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::paginate(10);
        return view('admin.role.index', compact('roles'));
    }

    /**
     * Show the form for creating a new role.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.role.add');
    }

    /**
     * Store a newly created role in role table.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'display_name' => 'nullable',
            'description' => 'nullable',
        ]);

        try {

            Role::create($request->all());

        } catch (\Exception $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }

        return redirect()->route('role.index')->with('message', 'نقش ساخته شد');

    }

    /**
     * Show the form for editing the role.
     *
     * @param Role $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        return view('admin.role.edit', compact('role'));
    }

    /**
     * Update the specified resource in role.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return void
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'name' => 'required',
            'display_name' => 'nullable',
            'description' => 'nullable',
        ]);

        Role::find($id)->update($request->all());

        return redirect()->route('role.index')->with('message', 'نقش به روز شد');

    }

    /**
     * Remove the specified resource from role.
     *
     * @param  int $id
     * @return void
     */
    public function destroy($id)
    {
        Role::find($id)->delete();

        return redirect()->route('role.index')->with('message', 'نقش پاک شد');
    }
}
