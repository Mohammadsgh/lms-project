<?php

namespace App\Http\Controllers\Admin\Role;

use App\Models\V1\Permission;
use App\Models\V1\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PermissionRoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request , [
            'id' => 'required|exists:roles'
        ]);

        $role = Role::find($request->input('id'));
        $role->syncPermissions($request->input('permission'));

        return redirect()->back()->with('message' , 'عملیات انجام شد');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return void
     */
    public function show($id)
    {
        $role = Role::find($id);
        $permissions = Permission::all(['id' , 'name' , 'display_name']);

        return view('admin.role.permission_role.add', compact('role' , 'permissions'));
    }

}
