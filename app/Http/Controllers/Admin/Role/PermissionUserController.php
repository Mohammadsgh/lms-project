<?php

namespace App\Http\Controllers\Admin\Role;

use App\Models\V1\Permission;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class PermissionUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        $permissionUsers = DB::table('permission_user')
            ->leftJoin('permissions' , 'permissions.id' , '=' , 'permission_user.permission_id')
            ->leftJoin('users' , 'users.id' , '=' , 'permission_user.user_id')
            ->select('permission_user.permission_id' , 'users.username' , 'permissions.display_name' , 'permission_user.user_id')
            ->paginate();

        return view('admin.role.permission_user.index' , compact('permissionUsers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        $users = User::all(['id' , 'username']);
        $permissions = Permission::all(['id' , 'display_name']);
        return view('admin.role.permission_user.add' , compact('users' , 'permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request , [
            'id' => 'required|exists:users'
        ]);

        $user = User::find($request->input('id'));

        $user->syncPermissions($request->input('permission'));

        return redirect()->route('permissionuser.index')->with('message' , 'دسترسی برای کاربر ساخته شد');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param $permissionId
     * @return void
     */
    public function destroy($permissionId , $userId)
    {
        $permissionUser = DB::table('permission_user')
            ->where('permission_id' , $permissionId)
            ->where('user_id' , $userId)
            ->delete();

        if ($permissionUser)
            return redirect()->route('permissionuser.index')->with('message' , 'دسترسی کاربر پاک شد');

        return redirect()->route('permissionuser.index')->with('error' , 'مشکل هنگام پاک کردن دسترسی');

    }
}
