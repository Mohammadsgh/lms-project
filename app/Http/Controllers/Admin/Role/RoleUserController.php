<?php

namespace App\Http\Controllers\Admin\Role;

use App\Models\V1\Role;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class RoleUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        $roleUsers = DB::table('role_user')
            ->leftJoin('roles' , 'roles.id' , '=' , 'role_user.role_id')
            ->leftJoin('users' , 'users.id' , '=' , 'role_user.user_id')
            ->select('role_user.role_id' , 'users.username' , 'roles.display_name' , 'role_user.user_id')
            ->paginate();

        return view('admin.role.role_user.index' , compact('roleUsers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        $users = User::all(['id' , 'username']);
        $roles = Role::all(['id' , 'display_name']);
        return view('admin.role.role_user.add' , compact('users' , 'roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    public function store(Request $request)
    {

        $this->validate($request , [
            'id' => 'required|exists:users'
        ]);

        $user = User::find($request->input('id'));

        $user->syncRoles($request->input('role'));

        return redirect()->route('admin.roleuser.index')->with('message' , 'دسترسی برای کاربر ساخته شد');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param $permissionId
     * @return void
     */
    public function destroy($roleId , $userId)
    {
        $permissionUser = DB::table('role_user')
            ->where('role_id' , $roleId)
            ->where('user_id' , $userId)
            ->delete();

        if ($permissionUser)
            return redirect()->route('admin.roleuser.index')->with('message' , 'نقش کاربر پاک شد');

        return redirect()->route('admin.roleuser.index')->with('error' , 'مشکل هنگام پاک کردن نقش');

    }
}
