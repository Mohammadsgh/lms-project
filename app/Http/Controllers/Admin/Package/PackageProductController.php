<?php

namespace App\Http\Controllers\Admin\Package;

use App\Http\Controllers\Controller;
use App\Models\V1\Package\Package;
use App\Models\V1\Product\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class PackageProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        Session::put('packageId', $id);

        $package = Package::find($id);

        $products = Product::whereHas('packages', function ($query) use ($id) {
            $query->where('packages.id', $id);
        })->paginate(10);

        return view('admin.package.package_product.index', compact('products', 'package'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        return view('admin.package.package_product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'package' => 'required|numeric',
            'products' => 'required',
            'products.*' => 'required',
        ]);

        $package = Package::findOrFail($request->input('package'));
        $package->products()->sync($request->input('products'));
        $packageId = Session::get('packageId');

        return redirect()->route('admin.packageproduct.list' , [$packageId])->with('message', 'محصول به پکیج افزوده شد');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Product $product
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */

    public function destroy(Product $product)
    {
        $packageId = Session::get('packageId');
        $product->delete();
        return redirect()->route('admin.packageproduct.list', [$packageId]);
    }

    public function packages()
    {
        return Package::all(['id', 'name as text']);
    }

    public function products()
    {
        return Product::all(['id', 'name as text']);
    }


}
