<?php

namespace App\Http\Controllers\Admin\Package;

use App\Helpers\ImageUpload;
use App\Http\Requests\PackageRequest;
use App\Models\V1\Package\Package;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        $packages = Package::paginate(10);
        return view('admin.package.index', compact('packages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        return view('admin.package.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\PackageRequest $request
     * @return void
     */
    public function store(PackageRequest $request)
    {

        $image = null;

        if ($request->hasFile('image')) {
            $image = ImageUpload::getInstance()
                ->files($request, ['image'])
                ->store('files/package/images')
                ->resize('-', 10, 20)
                ->get();
        }

        $data = $request->all();
        $data['user_id'] = Auth::user()->id;
        $data['image'] = $image[0]['path'];

        Package::create($data);

        return redirect()->route('admin.package.index')->with('message', 'پکیج ساخته شد');
    }

    /**
     * Display the specified resource.
     *
     * @param  Package $package
     * @return void
     */
    public function show(Package $package)
    {
        return view('admin.package.show', compact('package'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Package $package
     * @return void
     */
    public function edit(Package $package)
    {
        return view('admin.package.edit', compact('package'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\PackageRequest $request
     * @param  int $id
     * @return void
     */
    public function update(PackageRequest $request, $id)
    {

        $data = $request->all();
        $package = Package::findOrFail($id);

        if ($request->hasFile('image')) {

            @unlink($package->image);
            $image = ImageUpload::getInstance()
                ->files($request, ['image'])
                ->store('files/package/images')
                ->resize('-', 10, 20)
                ->get();

            $data['image'] = $image[0]['path'];
        }

        $package->update($data);

        return redirect()->route('admin.package.index')->with('message', 'پکیج به روز شد');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return void
     */
    public function destroy($id)
    {
        Package::find($id)->delete();
        return redirect()->route('admin.package.index')->with('message', 'پکیج پاک شد');
    }

}
