<?php

namespace App\Http\Controllers\Admin\Product;

use App\Models\V1\Product\ProductComment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Validator;

class ProductCommentController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param \Illuminate\Http\Request
     * @return void
     */
    public function index($id)
    {
        Session::put('productId', $id);

        $productComments = ProductComment::leftJoin('products', function ($join) {
            $join->on('products.id', '=', 'product_comments.product_id');
        })
            ->select('product_comments.firstname', 'product_comments.id',
                'product_comments.lastname', 'product_comments.email', 'product_comments.status', 'product_comments.created_at')
            ->paginate(10);

        return view('admin.product.product_comment.index', compact('productComments'));
    }

    /**
     * Accept Or Reject Product Of Comments.
     *
     * @param ProductComment $productComment
     * @param $type
     * @return void
     */
    public function getEditStatus(ProductComment $productComment, $type)
    {

        $productId = Session::get('productId');

        if ($type === 1)
            $productComment->update(['status' => 1]);

        $productComment->update(['status' => 1]);

        return redirect()->route('admin.productcomments.index' , [$productId])->with('message' , 'وضعیت دیدگاه محصول به روز شد');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return void
     */
    public function show($id)
    {
        $productComment = ProductComment::find($id);
        return view('admin.product.product_comment.edit', compact('productComment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return void
     */
    public function edit($id)
    {
        $productComment = ProductComment::find($id);
        return view('admin.product.product_comment.edit', compact('productComment'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ProductComment $productComment
     * @return integer Indicates the number of items.
     * @throws \Exception
     */
    public function destroy(ProductComment $productComment)
    {
        $productComment->delete();
        return redirect()->route('admin.productcomments.index', [Session::get('productId')])->with('message', 'گالری محصول پاک شد');
    }
}
