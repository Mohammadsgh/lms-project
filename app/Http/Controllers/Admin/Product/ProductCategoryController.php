<?php

namespace App\Http\Controllers\Admin\Product;

use App\Models\V1\Product\ProductCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        $productCategories = ProductCategory::paginate(10);
        return view('admin.product.product_category.index' , compact('productCategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        $productCategories = ProductCategory::all(['id' , 'name']);
        return view('admin.product.product_category.create' , compact('productCategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request , [
            'name' => 'required|string|unique:product_categories',
            'category_id' => 'required|numeric',
        ]);

        ProductCategory::create([
            'parent_id' => $request->input('category_id'),
            'name' => $request->input('name'),
        ]);

        return redirect()->route('admin.productcategories.index')->with('message' , 'دسته بندی محصول ساخته شد');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return void
     */
    public function edit($id)
    {
        $productCategory = ProductCategory::findOrFail($id);
        $productCategories = ProductCategory::all();
        return view('admin.product.product_category.edit' , compact('productCategory' , 'productCategories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        $this->validate($request , [
            'name' => 'required|string',
            'category_id' => 'required|numeric',
        ]);

        ProductCategory::find($id)->update([
            'parent_id' => $request->input('category_id'),
            'name' => $request->input('name'),
        ]);

        return redirect()->route('admin.productcategories.index')->with('message' , 'دسته بندی محصول به روز شد');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return void
     */
    public function destroy($id)
    {
        ProductCategory::find($id)->delete();
        return redirect()->route('admin.productcategories.index')->with('message' , 'دسته بندی محصول پاک شد');
    }

}
