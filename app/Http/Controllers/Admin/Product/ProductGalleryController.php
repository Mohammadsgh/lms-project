<?php

namespace App\Http\Controllers\Admin\Product;

use App\Models\V1\Product\ProductGallery;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\Helpers\ImageUpload;
use Validator;

class ProductGalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param \Illuminate\Http\Request
     * @return void
     */
    public function index($id)
    {
        Session::put('productId', $id);

        $productGalleries = DB::table('product_galleries')
            ->leftJoin('products', 'products.id', '=', 'product_galleries.product_id')
            ->where('product_galleries.product_id', $id)
            ->select('product_galleries.title', 'product_galleries.id',
                'product_galleries.thumbnail_image', 'product_galleries.image', 'product_galleries.alt')
            ->paginate(10);

        return view('admin.product.product_gallery.index', compact('productGalleries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        Session::get('productId');
        return view('admin.product.product_gallery.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    public function store(Request $request)
    {
        $productId = Session::get('productId');

        $this->validate($request, [
            'image' => 'required|array',
            'image.*' => 'required|image|mimes:jpeg,jpg,bmp,png|max:4000',
            'title.*' => 'nullable|string|max:255',
            'alt.*' => 'string|max:255',
        ]);

        $images = ImageUpload::getInstance()
            ->files($request, ['image'])
            ->store('files/product/images/product_gallery', true)
            ->thumb('-', 10, 20)
            ->get();

        foreach ($images as $key => $image) {

            ProductGallery::create([
                'product_id' => $productId,
                'title' => $request->input('title')[$key],
                'image' => $image['path'],
                'thumbnail_image' => $image['thumbs'],
                'alt' => $request->input('alt')[$key],
            ]);
        }

        return redirect()->route('admin.productgalleries.index', [$productId])->with('message', 'گالری محصول ساخته شد');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return void
     */
    public function edit($id)
    {
        $productGallery = ProductGallery::find($id);
        return view('admin.product.product_gallery.edit', compact('productGallery'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'image' => 'required|image|mimes:jpeg,jpg,bmp,png|max:4000',
            'title' => 'nullable|string|max:255',
            'alt' => 'nullable|string|max:255',
        ]);

        $data = $request->all();

        $productId = Session::get('productId');

        $productGallery = ProductGallery::find($id);

        if ($request->hasFile('image')) {

            @unlink($productGallery->image);
            @unlink($productGallery->thumbnail_image);

            $image = ImageUpload::getInstance()
                ->files($request, ['image'])
                ->store('files/product/images/product_gallery')
                ->thumb('-', 10, 20)
                ->get();

            $data['image'] = $image[0]['path'];
            $data['thumbnail_image'] = $image[0]['thumbs'];
        }

        $productGallery->update($data);

        return redirect()->route('admin.productgalleries.index', [$productId])->with('message', 'گالری محصول به روز شد');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ProductGallery $productGallery
     * @return integer Indicates the number of items.
     * @throws \Exception
     */
    public function destroy(ProductGallery $productGallery)
    {
        $productGallery->delete();
        @unlink($productGallery->image);
        @unlink($productGallery->thumbnail_image);
        return redirect()->route('admin.productgalleries.index', [Session::get('productId')])->with('message', 'گالری محصول به روز شد');
    }
}
