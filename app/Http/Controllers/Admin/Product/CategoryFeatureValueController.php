<?php

namespace App\Http\Controllers\Admin\Product;

use App\Helpers\ImageUpload;
use App\Models\V1\Product\CategoryFeature;
use App\Models\V1\Product\CategoryFeatureValue;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryFeatureValueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $featureValues = CategoryFeatureValue::paginate(10);
        return view('admin.product.category_feature_value.index', compact('featureValues'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $features = CategoryFeature::all(['id','name']);
        return view('admin.product.category_feature_value.create' , compact('features'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => 'required|string|max:255|unique:category_feature_values',
            'feature_id' => 'required|numeric',
            'image' => 'nullable|image|mimes:jpg,jpeg,png,bmp',
            'description' => 'nullable|string|max:1000',
        ]);

        $data = $request->all();

        if ($request->hasFile('image')) {

            $image = ImageUpload::getInstance()
                ->files($request, ['image'])
                ->store('files/product/features/category_feature_values')
                ->resize('-', 10, 20)
                ->get();

            $data['image'] = $image[0]['path'];
        }

        CategoryFeatureValue::create($data);

        return redirect()->route('admin.featurevalues.index')->with('message', 'مقدار ویژگی ساخته شد');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $featureValue = CategoryFeatureValue::find($id);
        $features = CategoryFeature::all(['id','name']);
        return view('admin.product.category_feature_value.edit', compact('featureValue' , 'features'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'feature_id' => 'required|numeric',
            'image' => 'nullable|image|mimes:jpg,jpeg,png,bmp',
            'description' => 'nullable|string|max:1000',
        ]);

        $data = $request->all();

        $featureValue = CategoryFeatureValue::find($id);

        if ($request->hasFile('image')) {
            @unlink($featureValue->image);
            @unlink($featureValue->thumbnail_image);

            $image = ImageUpload::getInstance()
                ->files($request, ['image'])
                ->store('files/product/images/product_gallery')
                ->resize('-', 10, 20)
                ->get();

            $data['image'] = $image[0]['path'];
        }

        $featureValue->update($data);

        return redirect()->route('admin.featurevalues.index')->with('message', 'ویژگی به روز شد');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param CategoryFeatureValue $featureVariant
     * @return void
     * @throws \Exception
     */
    
    public function destroy(CategoryFeatureValue $featureVariant)
    {
        @unlink($featureVariant->image);

        $featureVariant->delete();
        return redirect()->route('admin.featurevalues.index')->with('message', 'ویژگی به روز شد');
    }
}
