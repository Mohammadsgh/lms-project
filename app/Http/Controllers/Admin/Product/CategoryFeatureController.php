<?php

namespace App\Http\Controllers\Admin\Product;

use App\Models\V1\Product\CategoryFeature;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\ImageUpload;

class CategoryFeatureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $features = CategoryFeature::paginate(10);
        return view('admin.product.category_feature.index', compact('features'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.product.category_feature.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => 'required|string|max:255|unique:category_features',
            'image' => 'nullable|image|mimes:jpg,jpeg,png,bmp',
            'description' => 'nullable|string|max:1000',
        ]);

        $data = $request->all();

        if ($request->hasFile('image')) {

            $image = ImageUpload::getInstance()
                ->files($request, ['image'])
                ->store('files/product/features/category_features')
                ->resize('-', 10, 20)
                ->get();

            $data['image'] = $image[0]['path'];
        }

        CategoryFeature::create($data);

        return redirect()->route('admin.categoryfeatures.index')->with('message', 'ویژگی دسته بندی ساخته شد');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $feature = CategoryFeature::find($id);
        return view('admin.product.category_feature.edit', compact('feature'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'image' => 'nullable|image|mimes:jpg,jpeg,png,bmp',
            'description' => 'nullable|string|max:1000',
        ]);

        $data = $request->all();

        $feature = CategoryFeature::find($id);

        if ($request->hasFile('image')) {
            @unlink($feature->image);
            @unlink($feature->thumbnail_image);

            $image = ImageUpload::getInstance()
                ->files($request, ['image'])
                ->store('files/product/images/product_gallery')
                ->resize('-', 10, 20)
                ->get();

            $data['image'] = $image[0]['path'];
        }

        $feature->update($data);

        return redirect()->route('admin.categoryfeatures.index')->with('message', 'ویژگی به روز شد');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param CategoryFeature $categoryFeature
     * @return void
     * @throws \Exception
     */
    public function destroy(CategoryFeature $categoryFeature)
    {
        @unlink($categoryFeature->image);

        $categoryFeature->delete();
        return redirect()->route('admin.categoryfeatures.index')->with('message', 'ویژگی به روز شد');
    }
}
