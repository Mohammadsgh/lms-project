<?php

namespace App\Http\Controllers\Admin\Product;

use App\Models\V1\Product\CategoryFeature;
use App\Models\V1\Product\CategoryFeatureItem;
use App\Models\V1\Product\CategoryFeatureValue;
use App\Models\V1\Product\ProductCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryFeatureItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categoryFeatureItems = ProductCategory::whereHas('category_features')
            ->select('id', 'name')
            ->paginate(10);

        return view('admin.product.category_feature_item.index', compact('categoryFeatureItems'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $features = CategoryFeature::with(['feature_value:id,feature_id,name'])->get(['id', 'name']);
        return view('admin.product.category_feature_item.create', compact('features'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'categories.*' => 'required|numeric',
            'featureValues.*' => 'required|numeric',
        ]);

        $features = CategoryFeatureValue::with('feature')
            ->whereIn('id', $request->input('featureValues'))
            ->get(['id', 'feature_id']);


        foreach ($request->input('categories') as $categories) {
            foreach ($features as $feature) {
                CategoryFeatureItem::create([
                    'category_id' => $categories,
                    'feature_id' => $feature->feature_id,
                    'feature_value_id' => $feature->id,
                    'status' => 1
                ]);
            }
        }

        return redirect()->route('admin.featureitems.index')->with('message', 'ویژگی دسته بندی ساخته شد');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $feature = ProductCategory::whereHas('category_feature_values')->with(['category_feature_values' => function ($query) {
            $query->with('feature_value');
        }])->findOrFail($id);

        return view('admin.product.category_feature_item.show', compact('feature'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = ProductCategory::findOrFail($id);
        $allCategories = ProductCategory::all(['id', 'name']);
        $features = CategoryFeature::with(['feature_value:id,feature_id,name'])->get(['id', 'name']);
        $allFeatureValues = CategoryFeatureValue::pluck('id')->toArray();
        return view('admin.product.category_feature_item.edit', compact('features', 'category', 'allCategories', 'allFeatureValues'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        CategoryFeatureItem::whereIn('category_id' , $request->input(''));
//        $category->category_feature_values()->sync($request->input('categories'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        //
    }

    /**
     * Get All Product Categories.
     *
     * @param  void
     * @return \App\Models\V1\Product\ProductCategory
     */

    public function showCategories()
    {
        return ProductCategory::all(['id', 'name as text']);
    }

    public function showFeatures()
    {
        return CategoryFeature::all(['id', 'name as text']);
    }

    public function showFeatureValues()
    {
        return CategoryFeatureValue::all(['id', 'name as text']);
    }


}
