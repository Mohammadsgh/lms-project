<?php

namespace App\Http\Controllers\Admin\Product;

use App\Helpers\ImageUpload;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Models\V1\Product\Product;
use App\Models\V1\Product\ProductCategory;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        $products = Product::paginate(10);
        return view('admin.product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        $productCategories = ProductCategory::all(['id', 'name']);
        return view('admin.product.create', compact('productCategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\ProductRequest $request
     * @return void
     */
    public function store(ProductRequest $request)
    {
        $image = null;
        if ($request->hasFile('image')) {
            $image = ImageUpload::getInstance()
                ->files($request, 'image')
                ->store('files/product/images')
                ->resize('-', 10, 20)
                ->get();
        }

        $data = $request->all();
        $data['user_id'] = Auth::user()->id;
        $data['image'] = $image[0]['path'];

        Product::create($data);

        return redirect()->route('admin.product.index')->with('message', 'محصول ساخته شد');
    }

    /**
     * Display the specified resource.
     *
     * @param  Product $product
     * @return void
     */
    public function show(Product $product)
    {
        $productCategories = ProductCategory::all(['id', 'name']);
        return view('admin.product.show', compact('product', 'productCategories'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Product $product
     * @return void
     */
    public function edit(Product $product)
    {
        $productCategories = ProductCategory::all(['id', 'name']);
        return view('admin.product.edit', compact('product', 'productCategories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\ProductRequest $request
     * @param  int $id
     * @return void
     */
    public function update(ProductRequest $request, $id)
    {

        $data = $request->all();
        $product = Product::findOrFail($id);

        if ($request->hasFile('image')) {

            @unlink($product->image);
            
            $image = ImageUpload::getInstance()
                ->files($request, 'image')
                ->store('files/product/images')
                ->resize('-', 10, 20)
                ->get();

            $data['image'] = $image[0]['path'];
        }

        $product->update($data);

        return redirect()->route('admin.product.index')->with('message', 'محصول به روز شد');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return void
     */
    public function destroy($id)
    {
        Product::find($id)->delete();
        return redirect()->route('admin.product.index')->with('message', 'محصول پاک شد');
    }
}
