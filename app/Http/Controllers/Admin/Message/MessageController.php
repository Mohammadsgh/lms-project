<?php

namespace App\Http\Controllers\Admin\Message;

use App\Models\V1\Message\Message;
use App\Models\V1\Message\MessageContent;
use App\Models\V1\Message\MessageUser;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MessageController extends Controller
{

    /*function __construct()
    {
        checkRole(['admin']);
    }*/

    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        $messages = DB::table('message_users')
            ->leftJoin('messages', 'messages.id', '=', 'message_users.message_id')
            ->leftJoin('users', 'users.id', '=', 'message_users.target_id')
            ->leftJoin('message_contents', 'message_contents.message_id', '=', 'messages.id')
            ->select('message_users.id', 'messages.title', 'users.username', 'message_contents.content')
            ->paginate(10);

        return view('admin.message.index', compact('messages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        return view('admin.message.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'title' => 'required|string|max:255',
            'content' => 'required|string',
            'type' => 'in:1,2'
        ]);

        $message = Message::create([
            'title' => $request->input('title'),
            'status' => 1,
            'created_at' => date('Y-md H:i:s'),
            'updated_at' => date('Y-md H:i:s'),
        ]);

        if ($request->input('type') == 1) {

            MessageUser::create([
                'message_id' => $message->id,
                'target_id' => $request->input('user_id'),
                'user_id' => Auth::user()->id,
                'type' => 'single',

            ]);

        } else {

            foreach ($request->input('user_id') as $userId) {
                MessageUser::create([
                    'message_id' => $message->id,
                    'target_id' => $userId,
                    'user_id' => Auth::user()->id,
                    'type' => 'group',

                ]);
            }

        }

        MessageContent::create([
            'message_id' => $message->id,
            'content' => $request->input('content')
        ]);

        return redirect()->route('admin.message.index')->with('message', 'پیغام فرستاده شد');

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return void
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return void
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return void
     */
    public function destroy($id)
    {
        //
    }
}
