<?php

namespace App\Http\Controllers\Profile\Message;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        checkRole(['teacher', 'student', 'parents']);

        $userId = Auth::user()->id;

        $messages = DB::table('message_users')
            ->leftJoin('messages', 'messages.id', '=', 'message_users.message_id')
            ->leftJoin('message_contents', 'message_contents.message_id', '=', 'messages.id')
            ->select('message_users.id', 'messages.title', 'message_contents.content', 'messages.created_at')
            ->where('message_users.target_id', $userId)
            ->paginate(10);

        return view('profile.message.message', compact('messages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return void
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return void
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return void
     */
    public function destroy($id)
    {
        //
    }
}
