<?php


/**
 * create and manage products route
 */

Route::resource('products/product' , 'Product\ProductController');
Route::get('products/product/destroy/{id}' , 'Product\ProductController@destroy')->name('product.destroy');


/**
 * create and manage product categories route
 */

Route::resource('products/productcategories' , 'Product\ProductCategoryController');
Route::get('products/productcategories/destroy/{id}' , 'Product\ProductCategoryController@destroy')->name('productcategories.destroy');

/**
 * create and manage product Galleries route
 */

Route::resource('products/productgalleries' , 'Product\ProductGalleryController');
Route::get('products/productgalleries/destroy/{productGallery}' , 'Product\ProductGalleryController@destroy')->name('productgalleries.destroy');
Route::get('products/productgalleries/list/{id}' , 'Product\ProductGalleryController@index')->name('productgalleries.index');

/**
 * create and manage product Category Features route
 */

Route::resource('products/categoryfeatures' , 'Product\CategoryFeatureController');
Route::get('products/categoryfeatures/destroy/{categoryFeature}' , 'Product\CategoryFeatureController@destroy')->name('categoryfeatures.destroy');

/**
 * create and manage product Category Feature Values route
 */

Route::resource('products/featurevalues' , 'Product\CategoryFeatureValueController');
Route::get('products/featurevalues/destroy/{featureVariant}' , 'Product\CategoryFeatureValueController@destroy')->name('featurevalues.destroy');

/**
 * create and manage product Category feature Items route
 */

Route::get('products/categories/featureitems/showcategories' , 'Product\CategoryFeatureItemController@showCategories')->name('featureitems.showcategories');
Route::get('products/categories/featureitems/showfeatures' , 'Product\CategoryFeatureItemController@showFeatures')->name('featureitems.showfeatures');
Route::get('products/categories/featureitems/showfeaturevalues' , 'Product\CategoryFeatureItemController@showFeatureValues')->name('featureitems.showfeaturevalues');
Route::resource('products/categories/featureitems' , 'Product\CategoryFeatureItemController');
Route::get('products/categories/featureitems/destroy/{featureItem}' , 'Product\CategoryFeatureItemController@destroy')->name('featureitems.destroy');
