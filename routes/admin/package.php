<?php


/**
 * create and manage packages route
 */

Route::resource('packages/package' , 'Package\PackageController');
Route::get('packages/package/destroy/{id}' , 'Package\PackageController@destroy')->name('package.destroy');

/**
 * create and manage package Products route
 */

Route::get('packages/packageproduct/packages' , 'Package\PackageProductController@packages')->name('packageproduct.packages');
Route::get('packages/packageproduct/products' , 'Package\PackageProductController@products')->name('packageproduct.products');
Route::resource('packages/packageproduct' , 'Package\PackageProductController');
Route::get('packages/packageproduct/list/{id}' , 'Package\PackageProductController@index')->name('packageproduct.list');
Route::get('packages/packageproduct/destroy/{product}' , 'Package\PackageProductController@destroy')->name('packageproduct.destroy');
