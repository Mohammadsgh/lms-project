<?php


/**
 * create and manage Messages route
 */

Route::resource('messages/message' , 'Message\MessageController');
Route::get('messages/messagecategory/messagecategories' , 'Message\MessageCategoryController@messageCategories')->name('admin.messagecategory.messageCategories');
Route::get('messages/message/destroy/{id}' , 'Message\MessageController@destroy')->name('admin.message.destroy');
