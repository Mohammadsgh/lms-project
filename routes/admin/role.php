<?php


/**
 * create and Manage role route
 */

Route::resource('roles/role' , 'Role\RoleController');
Route::get('roles/role/destroy/{id}' , 'Role\RoleController@destroy')->name('admin.role.destroy');


/**
 * create and Manage permission route
 */

Route::resource('permissions/permission' , 'Role\PermissionController');
Route::get('permissions/permission/destroy/{id}' , 'Role\PermissionController@destroy')->name('admin.permission.destroy');

/**
 * create and Manage permission role route
 */

Route::resource('permissions/permissionrole' , 'Role\PermissionRoleController');


/**
 * create and Manage permission User route
 */

Route::resource('permissions/permissionuser' , 'Role\PermissionUserController');
Route::get('permissions/permissionuser/destroy/{permission_id}/{user_id}' , 'Role\PermissionUserController@destroy')->name('admin.permissionuser.destroy');


/**
 * create and Manage Role User route
 */

Route::resource('roles/roleuser' , 'Role\RoleUserController');
Route::get('roles/roleuser/destroy/{role_id}/{user_id}' , 'Role\RoleUserController@destroy')->name('roleuser.destroy');








